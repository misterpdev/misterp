from django.contrib import admin
from .models import *


class SoftwareFields(admin.ModelAdmin):
    """
    Here we will display some selected fields
    """
    list_display = ('software_name',
                    'software_version',
                    'architecture',
                    'number_of_licence',
                    'licence_start_date',
                    'licence_end_date')


admin.site.register(Software, SoftwareFields)


class ComputerFields(admin.ModelAdmin):
    """
    Here we will display all available fields
    """
    list_display = [f.name for f in Computer._meta.fields]


admin.site.register(Computer, ComputerFields)


class CurrencyFields(admin.ModelAdmin):
    """
    Here we will display all available fields
    """
    list_display = [f.name for f in Currency._meta.fields]


admin.site.register(Currency, CurrencyFields)


class DeskFields(admin.ModelAdmin):
    """
    Here we will display all available fields
    """
    list_display = [f.name for f in DeskNumber._meta.fields]


admin.site.register(DeskNumber, DeskFields)


class AccessCardFields(admin.ModelAdmin):
    """
    Here we will display all available fields
    """
    list_display = [f.name for f in AccessCard._meta.fields]


admin.site.register(AccessCard, AccessCardFields)
