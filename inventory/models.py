from __future__ import unicode_literals
from django.db import models
from hrm.models import Employee


class Currency(models.Model):
    name = models.CharField(max_length=100, null=True, unique=True)
    code = models.CharField(max_length=10, null=True, unique=True)

    def __str__(self):
        return self.name


class DeskNumber(models.Model):
    desk_code = models.CharField(max_length=10,  unique=True)
    comment = models.CharField(max_length=100, null=True, blank=True)
    assign_to = models.ForeignKey(Employee, on_delete=models.CASCADE, null=True, blank=True)

    def __str__(self):
        return "{} {}".format(self.desk_code, self.assign_to)


class AccessCard(models.Model):
    card_number = models.CharField(max_length=10, null=True, unique=True)
    comment = models.CharField(max_length=100, null=True, blank=True)
    assign_to = models.ForeignKey(Employee, on_delete=models.CASCADE, null=True, blank=True)

    def __str__(self):
        return self.card_number


class Software(models.Model):
    software_name = models.CharField(max_length=200, default='')
    software_version = models.CharField(max_length=200, default='')
    software_url = models.URLField(max_length=200, null=True, blank=True)
    l_type = (('network', 'Network'),
              ('single', 'Single'),
              ('community', 'Community'),
              ('opensource', 'Open Source'),
              ('Free', 'Free')
              )
    licence_type = models.CharField(max_length=50, choices=l_type, null=True, blank=True)
    arch = (('64bit', '64 Bit'),
            ('32bit', '32 Bit'),
            )
    architecture = models.CharField(max_length=10, choices=arch, null=True, blank=True)
    vendor_name = models.CharField(max_length=50, null=True, blank=True)
    vendor_email = models.EmailField(max_length=50, null=True, blank=True)
    vendor_phone = models.CharField(max_length=20, null=True, blank=True)
    licence_cost = models.FloatField(null=True, blank=True)
    licence_currency = models.ForeignKey(Currency, on_delete=models.CASCADE, related_name='+', null=True, blank=True)
    licence_start_date = models.DateField(null=True, blank=True)
    licence_end_date = models.DateField(null=True, blank=True)
    licence_mac_address = models.CharField(max_length=50, null=True, blank=True)
    licence_server_ip = models.GenericIPAddressField(protocol='both', null=True, blank=True)
    licence_duration = models.CharField(max_length=50, null=True, blank=True)
    number_of_licence = models.IntegerField(null=True, blank=True)
    request_by = models.ForeignKey(Employee, on_delete=models.CASCADE, related_name='+', null=True, blank=True)
    request_date = models.DateField(null=True, blank=True)
    approved_by = models.ForeignKey(Employee, on_delete=models.CASCADE, related_name='+', null=True, blank=True)
    approved_date = models.DateField(null=True, blank=True)
    purchased_by = models.ForeignKey(Employee, on_delete=models.CASCADE, related_name='+', null=True, blank=True)
    purchased_date = models.DateField(null=True, blank=True)

    def __str__(self):
        return self.software_name


class Computer(models.Model):
    asset_tag = models.CharField(max_length=100, default='computer_tag', unique=True)
    hostname = models.CharField(max_length=100, null=True, blank=True)
    ip_address = models.GenericIPAddressField(max_length=100, null=True, blank=True)
    mac_address = models.CharField(max_length=100, null=True, blank=True)
    processor = models.CharField(max_length=100, null=True, blank=True)
    core = models.CharField(max_length=100, null=True, blank=True)
    memory = models.CharField(max_length=100, null=True, blank=True)
    graphics_card = models.CharField(max_length=100, null=True, blank=True)
    motherboard = models.CharField(max_length=100, null=True, blank=True)
    motherboard_serial = models.CharField(max_length=100, null=True, blank=True)
    hdd0 = models.CharField(max_length=100, null=True, blank=True)
    hdd1 = models.CharField(max_length=100, null=True, blank=True)
    hdd2 = models.CharField(max_length=100, null=True, blank=True)
    monitor1 = models.CharField(max_length=100, null=True, blank=True)
    monitor1_serial = models.CharField(max_length=100, null=True, blank=True)
    monitor2 = models.CharField(max_length=100, null=True, blank=True)
    monitor2_serial = models.CharField(max_length=100, null=True, blank=True)
    os_type = (('win', 'Windows'),
               ('linux', 'Linux'),
               ('mac', 'Mac'),
               )
    os = models.CharField(max_length=100, choices=os_type, null=True, blank=True)
    cost = models.FloatField(default=1.0, null=True, blank=True)
    desk = models.ForeignKey(DeskNumber, on_delete=models.CASCADE, null=True, blank=True)

    def __str__(self):
        return self.asset_tag
