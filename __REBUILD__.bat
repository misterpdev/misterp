echo "Deleting migration files"
python scripts/clear_migrations.py

echo "BUILDING TABLES"
python manage.py makemigrations

echo "MIGRATION DATABASE"
python manage.py migrate

echo "DATABASE BUILD SUCESSFULLY"

echo "LOADING DATA FROM FIXTURES"
python manage.py loaddata --app auth auth.json
python manage.py loaddata --app authtoken authtoken.json
python manage.py loaddata --app hrm hrm.json
python manage.py loaddata --app production production.json
echo "DATA ADDED SUCESSFULLY"

echo "Super user details - Username: admin,  Password: mist1234"

pause