![alt text](https://github.com/vfxpandit/mist-api/blob/master/icon.png)

#  Mist Studio API

The Core Application which contain
- Database Operations
- Rest API Interface
- Backend Logics
- User Profiles, Authentication, Groups & Permissions 

### Requirements

Python 3.6.8  - 64bit

### Python Packages
* psutil==5.4.7
* Django==2.1.5
* PySide2==5.12.0
* requests==2.21.0
* djangorestframework==3.9.1
* mysqlclient==1.3.12
* Pillow==5.4.1
* django-model-utils==3.1.2
* django-imagekit==4.0.2
* django-debug-toolbar==1.11
* python-memcached==1.59
* matplotlib==3.0.2
* tornado==5.1.1
* pyinstaller==3.4
* pymongo==3.7.2


### How to run Mist API Server
#### Clone this repository
git clone https://github.com/vfxpandit/mist-api
#### Go into the repository
cd mist-api
#### Install dependencies
pip install -r requirements.txt
#### Makemigrations and Migrate
python manage.py makemigrations
python manage.py migrate
#### run server
python manage.py runserver


### Licence
````
Copyright (c) 2019, MIST VFX(www.mistvfxstudio.com) . All rights reserved.
Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:

    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.

    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.

    * Neither the name of MIST VFX(www.mistvfxstudio.com) nor the names of any
      other contributors to this software may be used to endorse or
      promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
````
