from django.contrib import admin
from production.models import *

# Register your models here.

admin.site.register(ProjectCrew)

admin.site.register(CrewRole)

admin.site.register(Client)

admin.site.register(Task)

admin.site.register(ProjectType)

admin.site.register(ProjectStage)

admin.site.register(FileStatus)

admin.site.register(Complexity)

admin.site.register(PackageType)

admin.site.register(Package)

admin.site.register(ShotName)


class ShotTaskFields(admin.ModelAdmin):
    list_display = [f.name for f in ShotTask._meta.fields]


admin.site.register(ShotTask, ShotTaskFields)

admin.site.register(ShotTaskVersion)

admin.site.register(ShotTaskBrief)


class ProjectFields(admin.ModelAdmin):
    list_display = [f.name for f in Project._meta.fields]
    list_display.insert(0, 'project_poster')
    search_fields = ('name', 'project_type', 'project_stage')


admin.site.register(Project, ProjectFields)
