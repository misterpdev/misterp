from rest_framework.views import APIView
from rest_framework.response import Response
from .serializers import ProjectSerializer, FileStatusSerializer
from rest_framework import status
from .models import *


class ProjectsAPI(APIView):
    """
    Get all projects and Create New Project
    """
    def get(self, request, format=None):
        model_object = Project.objects.all()
        serializer = ProjectSerializer(model_object, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        serializer = ProjectSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ProjectsInfoAPI(APIView):
    """
    Get project information by project_id and update project information
    """

    def get(self, request, project_id, format=None):
        model_object = Project.objects.get(id=project_id)
        serializer = ProjectSerializer(model_object)
        return Response(serializer.data)

    def put(self, request, project_id, format=None):
        model_object = Project.objects.get(id=project_id)
        serializer = ProjectSerializer(model_object, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class FileStatusAPI(APIView):
    """
    File Status API
    """
    def get(self, request, format=None):
        serializer = FileStatusSerializer(FileStatus.objects.all(), many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        serializer = FileStatusSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)