from django.shortcuts import render
from .models import *


def project_view(request):
    """
    Project View
    :param request:
    :return:
    """
    context = {
               'project_type': ProjectType.objects.all(),
               'project_stage': ProjectStage.objects.all(),
               'tasks': Task.objects.all()
               }
    return render(request, 'production/projects.html', context)


def project_data(request, project_id):
    """
    Project data View
    :param request:
    :param project_id:
    :return:
    """
    context = {'project': Project.objects.get(id=project_id) }
    return render(request, 'production/project-data.html', context)


def production_settings(request):
    return render(request, 'production/settings.html')


def my_task(request):
    return render(request, 'production/my_task.html')


def dailies(request):
    return render(request, 'production/dailies.html')


def planner(request):
    return render(request, 'production/planner.html')


def reports(request):
    return render(request, 'production/reports.html')
