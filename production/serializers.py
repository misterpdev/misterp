from rest_framework import serializers
from .models import *


class ProjectSerializer(serializers.ModelSerializer):
    name = serializers.CharField(required=False)
    project_type = serializers.SlugRelatedField(queryset=ProjectType.objects.all(), slug_field='name', required=False)
    project_stage = serializers.SlugRelatedField(queryset=ProjectStage.objects.all(), slug_field='name', required=False)
    task = serializers.SlugRelatedField(queryset=Task.objects.all(), slug_field='name',many=True, required=False)

    class Meta:
        model = Project
        fields = '__all__'
        depth = 1


class FileStatusSerializer(serializers.ModelSerializer):

    class Meta:
        model = FileStatus
        fields = '__all__'
