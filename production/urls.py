from django.conf.urls import url
from . import api, views

urlpatterns = [
    url(r'^production/project/$', views.project_view, name='projects_list'),
    url(r'^production/project/(?P<project_id>\d+)/$', views.project_data, name='projects_data'),
    url(r'^production/mytask/$', views.my_task, name='my_task'),
    url(r'^production/dailies/$', views.dailies, name='dailies'),
    url(r'^production/planner/$', views.planner, name='planner'),
    url(r'^production/reports/$', views.reports, name='reports'),
    url(r'^production/settings/$', views.production_settings, name='production_settings'),
    url(r'^api/production/project/$', api.ProjectsAPI.as_view(), name='projects'),
    url(r'^api/production/project/(?P<project_id>\d+)/$', api.ProjectsInfoAPI.as_view(), name='project_info'),
    url(r'^api/production/filestatus/$', api.FileStatusAPI.as_view(), name='project_info'),
]
