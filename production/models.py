from django.db import models
from django.utils.safestring import mark_safe
from imagekit.models import ProcessedImageField
from imagekit.processors import ResizeToFill
from model_utils import fields
from hrm.models import Employee, Location, Level


class Client(models.Model):
    name = models.CharField(max_length=255)
    phone = models.CharField(max_length=255, null=True, blank=True)
    address = models.CharField(max_length=255, null=True, blank=True)
    email = models.EmailField(max_length=255, null=True, blank=True)

    def __str__(self):
        return self.name


class Priority(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name


class Complexity(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name


class Task(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name


class ProjectType(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name


class ProjectStage(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name


class FileStatus(models.Model):
    """
     ('new', 'New'),
     ('wip', 'Wip'),
     ('not_started', 'Not Started'),
     ('published', 'Published'),
     ('feedback', 'Feedback'),
     ('approved', 'Approved')

    """
    name = models.CharField(max_length=50, unique=True)

    def __str__(self):
        return self.name


class Project(models.Model):
    name = models.CharField(max_length=255, unique=True)
    task = models.ManyToManyField(Task)
    project_stage = models.ForeignKey(ProjectStage, on_delete=models.CASCADE, related_name='+', null=True, blank=True)
    project_type = models.ForeignKey(ProjectType, on_delete=models.CASCADE, related_name='+', null=True, blank=True)
    package_type = models.ManyToManyField('PackageType')

    def upload_poster_dir(self, filename):
        path = 'production/projects/{}/poster/{}'.format(self.id, filename)
        return path

    poster = ProcessedImageField(upload_to=upload_poster_dir,
                                 processors=[ResizeToFill(200, 300)],
                                 format='JPEG',
                                 options={'quality': 80},
                                 null=True
                                 )

    description = models.CharField(max_length=255, null=True, blank=True)
    client = models.ForeignKey(Client, on_delete=models.CASCADE, related_name='+', null=True, blank=True)
    production_path = models.CharField(max_length=255, null=True, blank=True)
    checkout_path = models.CharField(max_length=255, null=True, blank=True)
    backup_path = models.CharField(max_length=255, null=True, blank=True)
    project_start_date = models.DateField(null=True, blank=True)
    project_end_date = models.DateField(null=True, blank=True)
    creation_date = models.DateTimeField(auto_now_add=True)
    modified_date = models.DateTimeField(auto_now=True)

    def project_poster(self):
        if self.poster:
            return mark_safe('<img src="%s" style="width: 45px; height:45px;" />' % self.poster.url)
        else:
            return 'No Image Found'

    poster.short_description = 'Image'

    def __str__(self):
        return self.name


class PackageType(models.Model):
    """
    PACKAGE, SEQUENCE
    """
    name = models.CharField(max_length=255, unique=True)

    def __str__(self):
        return self.name


class Package(models.Model):
    """
    Pkg01, Pkg02
    """
    project = models.ForeignKey(Project, on_delete=models.CASCADE)
    package_type = models.ForeignKey(PackageType, on_delete=models.CASCADE, related_name='+')
    name = models.CharField(max_length=100)
    description = models.TextField(null=True, blank=True)
    date_of_turnover = models.DateField()
    creation_time = models.DateTimeField(auto_now_add=True)
    modified_time = models.DateTimeField(auto_now=True, null=True)

    def __str__(self):
        return "{}_{}_{}".format(self.project.name, self.package_type, self.name)


class ShotName(models.Model):
    """
    SHOT NAME = ABC_001, ABC_002
    """
    package = models.ForeignKey(Package, on_delete=models.CASCADE)
    name = models.CharField(max_length=255)
    description = models.TextField(null=True, blank=True)
    creation_time = models.DateTimeField(auto_now_add=True)
    modified_time = models.DateTimeField(auto_now=True, null=True)

    def __str__(self):
        return self.name


class ShotTask(models.Model):
    shot_name = models.ForeignKey(ShotName, on_delete=models.CASCADE)
    task = models.ForeignKey(Task, on_delete=models.CASCADE)
    client_bid = models.FloatField(null=True, blank=True)
    artist_bid = models.FloatField(null=True, blank=True)
    plate_name = models.CharField(max_length=255, null=True, blank=True)
    start_frame = models.FloatField(null=True, blank=True)
    end_frame = models.FloatField(null=True, blank=True)
    total_frame = models.FloatField(null=True, blank=True)
    complexity = models.ForeignKey(Complexity, on_delete=models.CASCADE, related_name='+', null=True, blank=True)
    location = models.ForeignKey(Location, on_delete=models.CASCADE, related_name='+', null=True, blank=True)
    level = models.ForeignKey(Level, on_delete=models.CASCADE, related_name='+', null=True, blank=True)
    comment = models.TextField(null=True, blank=True)
    status = models.ForeignKey(FileStatus, on_delete=models.CASCADE, null=True, blank=True)
    progress = models.FloatField(null=True, blank=True)
    start_date = models.DateField(null=True, blank=True)
    end_date = models.DateField(null=True, blank=True)
    expected_delivery_date = models.DateField(null=True, blank=True)
    client_eta = models.DateField(null=True, blank=True)
    mist_eta = models.DateField(null=True, blank=True)

    priority_choice = (('Low', 'Low'),
                       ('Normal', 'Normal'),
                       ('Medium', 'Medium'),
                       ('High', 'High'),
                       ('Critical', 'Critical')
                       )
    priority = models.CharField(max_length=50, choices=priority_choice, null=True, blank=True)
    created_by = models.ForeignKey(Employee, on_delete=models.CASCADE, related_name='created_by', null=True, blank=True)
    creation_time = models.DateTimeField(auto_now_add=True)
    modified_time = models.DateTimeField(auto_now=True, null=True)
    assign_to = models.ForeignKey(Employee, on_delete=models.CASCADE, related_name='assign_to', null=True, blank=True)
    assign_date = fields.MonitorField(monitor='assign_to', null=True, blank=True)
    assign_by = models.ForeignKey(Employee, on_delete=models.CASCADE, related_name='assign_by', null=True, blank=True)

    actual_start_date = fields.MonitorField(monitor='status', when=[5], null=True, blank=True)
    actual_end_date = models.DateTimeField(null=True, blank=True)
    approval_date = fields.MonitorField(monitor='status', when=[1], null=True, blank=True)

    def upload_thumb_dir(self, filename):
        path = 'production/projects/{}/{}/thumb/{}'.format(self.shot_name.package.project.id, self.id, filename)
        return path

    thumb = models.ImageField(upload_to=upload_thumb_dir, default='/icons/default/photo.png', blank=True)

    def __str__(self):
        return "{}_{}_{}".format(self.shot_name.package.project, self.shot_name, self.task.name)


class ShotTaskVersion(models.Model):
    """
    Table for store shots Versions
    """
    shot_task = models.ForeignKey(ShotTask, on_delete=models.CASCADE)
    version = models.CharField(max_length=4, blank=True)  # V001

    def save(self, force_insert=False, force_update=False, *args, **kwargs):
        if self.version == "":
            existing_version = ShotTaskVersion.objects.all().filter(shot_task=self.shot_task.id).order_by(
                '-version')
            if existing_version.count() > 0:
                new_version = int(existing_version[0].version[1:]) + 1
            else:
                new_version = 0 + 1
            self.version = 'V%03d' % new_version
        super(ShotTaskVersion, self).save(*args, **kwargs)

    checkout_by = models.ForeignKey(Employee, on_delete=models.CASCADE, related_name='checkout_by', null=True,
                                    blank=True)
    checkout_time = models.DateTimeField(null=True, blank=True)
    checkout_path = models.CharField(max_length=255, null=True, blank=True)
    checkout_comment = models.CharField(max_length=255, null=True, blank=True)

    publish_by = models.ForeignKey(Employee, on_delete=models.CASCADE, related_name='publish_by', null=True, blank=True)
    publish_time = models.DateTimeField(null=True, blank=True)
    publish_path = models.CharField(max_length=255, null=True, blank=True)
    publish_comment = models.CharField(max_length=255, null=True, blank=True)
    is_final = models.NullBooleanField(default=False, blank=True)
    status = models.ForeignKey(FileStatus, on_delete=models.CASCADE, related_name='+', default=4, null=True, blank=True)
    creation_time = models.DateTimeField(auto_now_add=True, null=True)

    def upload_thumb_dir(self, filename):
        path = 'production/projects/{}/{}/{}/{}/preview/{}'.format(
            self.shot_task.shot_name.package.project.id,
            self.shot_task.shot_name.id,
            self.shot_task.id,
            self.id, filename)
        return path

    thumb = models.ImageField(upload_to=upload_thumb_dir, default='/icons/default/photo.png', blank=True)

    def __str__(self):
        return '{}_{}_{}_{}'.format(
            self.shot_task.shot_name.package.project,
            self.shot_task.shot_name,
            self.shot_task,
            self.version
        )


class ShotTaskBrief(models.Model):
    shot_task = models.ForeignKey(ShotTask, on_delete=models.CASCADE, related_name='+')
    comment = models.TextField()

    def comment_media_dir(self, filename):
        path = 'production/projects/{}/{}/{}/{}/comments/{}'.format(
            self.shot_task.id,
            self.shot_task.shot_name.id,
            self.shot_task.shot_name.package.project.id,
            self.id, filename)
        return path

    file = models.ImageField(upload_to=comment_media_dir, null=True, blank=True)

    added_by = models.ForeignKey(Employee, on_delete=models.CASCADE, related_name='+')
    creation_date = models.DateTimeField(auto_now_add=True)
    modified_date = models.DateTimeField(auto_now=True)


class CrewRole(models.Model):
    name = models.CharField(max_length=255, unique=True)

    def __str__(self):
        return self.name


class ProjectCrew(models.Model):
    project = models.ForeignKey(Project, on_delete=models.CASCADE)
    crew = models.ForeignKey(Employee, on_delete=models.CASCADE, related_name='+')
    role = models.ForeignKey(CrewRole, on_delete=models.CASCADE, related_name='+')
    in_date = models.DateField()
    out_date = models.DateField()
    creation_time = models.DateTimeField(auto_now_add=True)
    modified_time = models.DateTimeField(auto_now=True, null=True)

    def __str__(self):
        return "{}_{}".format(self.project.name, self.crew.full_name)
