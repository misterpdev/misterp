from django.db import models
from django.contrib.auth.models import User


# Create your models here.
class Profile(models.Model):
    """
    User Profile
    """
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    account_creation_date = models.DateTimeField(auto_now_add=True)
    password_changed = models.BooleanField(default=False, blank=True)

    def __str__(self):
        return self.user.username


class SystemUsage(models.Model):
    """
    Database table to store system tracking data
    """
    profile = models.ForeignKey(Profile, on_delete=models.CASCADE)
    window = models.CharField(max_length=255)
    process_name = models.CharField(max_length=255)
    process_id = models.IntegerField()
    start_time = models.DateTimeField()
    end_time = models.DateTimeField()
    total_time = models.FloatField()

    def __str__(self):
        return "{} {} {}".format(self.profile.user, self.window, self.process_name)


class Attendance(models.Model):
    """
    Employee door based attendance
    """
    user = models.ForeignKey(User, on_delete=models.CASCADE, to_field='username')
    event_time = models.DateTimeField()
    event_type = models.CharField(max_length=5)  # In / Out
    event_point = models.CharField(max_length=255)
    event_description = models.CharField(max_length=255)

    def __str__(self):
        return "{} {} {} {}".format(self.event_point, self.event_type, self.event_description, self.user.username)


class AttendanceImport(models.Model):
    """
    This model will store Punch in records excel data file import dates
    """
    upload_chennai = models.DateField(blank=True, null=True)
    upload_cochin = models.DateField(blank=True, null=True)
    creation_date = models.DateTimeField(auto_now_add=True)
    modified_date = models.DateTimeField(auto_now=True)


class DailyWorkingHour(models.Model):
    """
    Employee daily working hours
    """
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='+')
    date = models.DateField()
    total = models.TimeField()
    actual = models.TimeField()
    non_completed = models.TimeField()
    additional = models.TimeField(null=True, blank=True)
    creation_date = models.DateTimeField(auto_now_add=True)
    modified_date = models.DateTimeField(auto_now=True)