from django.contrib import admin
from profiles.models import *

admin.site.register(SystemUsage)
admin.site.register(AttendanceImport)
admin.site.register(DailyWorkingHour)

class ProfileFields(admin.ModelAdmin):
    list_display = [f.name for f in Profile._meta.fields]
    exclude = ("user",)


admin.site.register(Profile, ProfileFields)


class AttendanceFields(admin.ModelAdmin):
    search_fields = ('employee__official_name', 'in_time', 'out_time')
    list_display = [f.name for f in Attendance._meta.fields]


admin.site.register(Attendance, AttendanceFields)
