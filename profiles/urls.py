from django.conf.urls import url, include
from . import api, views
import scripts

urlpatterns = [
    url(r'^login', views.login_view, name='login'),
    url(r'^logout', views.logout_view, name='logout'),
    url(r'^users/$', views.user_list, name='user_list'),
    url(r'^profile/$', views.profile_view, name='profile_view'),
    url(r'^profile/(?P<user_id>\d+)/$', views.other_profile_view, name='other_profile_view'),
    url(r'^profile/system-usage/(?P<user_id>\d+)/$', views.system_usage, name='system_usage'),
    url(r'^profile/performance/(?P<user_id>\d+)/$', views.performance, name='performance'),
    url(r'^profile/timeline/(?P<user_id>\d+)/$', views.timeline, name='timeline'),
    url(r'^api/users/$', api.UserList.as_view(), name='user_list_api'),
    url(r'^api/users/(?P<user_id>\d+)$', api.UserEdit.as_view()),
    url(r'^api/profile/(?P<profile_id>\d+)$', api.UserProfileAPIView.as_view()),
    url(r'^api/system-tracking/(?P<profile_id>\d+)$', api.SystemTrackerAPI.as_view()),
    url(r'^api/auth/$', api.UserAuthentication.as_view(), name='User Authentication API'),
    url(r'^api/attendance/(?P<username>[\w.@+-]+)/$', api.AttendanceAPI.as_view()),
    url(r'^api/work-hours/(?P<username>[\w.@+-]+)/$', api.DailyWorkingHourAPI.as_view()),
    url(r'^api/attendance-imports/$', api.AttendanceImportsAPI.as_view()),
]
