from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.authtoken.models import Token
from rest_framework.views import APIView
from rest_framework.response import Response
from django.contrib import auth
from django.contrib.auth.models import User
from django.http import Http404
from rest_framework import status
from .models import Profile, SystemUsage, Attendance, AttendanceImport, DailyWorkingHour
from hrm.models import Employee
from .serializers import UserProfileSerializer, UserSerializer, SystemTrackerSerializer, AttendanceSerializer, AttendanceImportSerializer, DailyWorkingHourSerializer
from scripts import work_hours


import datetime


class UserAuthentication(ObtainAuthToken):
    """
    This class will return user authentication
    """

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data,
                                           context={'request': request})
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        token, created = Token.objects.get_or_create(user=user)
        username = request.POST.get('username', '')
        password = request.POST.get('password', '')
        user = auth.authenticate(username=username, password=password)
        if user:
            profile = Profile.objects.get(user__username=username)
            photo = profile.employee.photo
            if photo:
                photo = profile.employee.photo.url
            else:
                photo = '/media/hrm/employees/photo/default.jpg'
            data = {
                'token': token.key,
                'id': profile.employee.id,
                'full_name': profile.employee.full_name,
                'employee_id': profile.employee.employee_id,
                'photo': photo,
                'groups': profile.user.groups.all().values_list()
            }
            return Response(data)
        else:
            return Response(False)


class UserList(APIView):
    def get(self, request, format=None):
        serializer = UserSerializer(User.objects.all(), many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        serializer = UserSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class UserEdit(APIView):
    def get(self, request, user_id, format=None):
        serializer = UserSerializer(User.objects.get(id=user_id))
        return Response(serializer.data)

    def put(self, request, user_id):
        serializer = UserProfileSerializer(User.objects.get(id=user_id), data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, user_id, format=None):
        model_object = User.objects.get(id=user_id)
        model_object.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class UserProfileAPIView(APIView):
    """
    This class will return json data of user profile
    """

    def get_object(self, profile_id):
        try:
            return Profile.objects.get(pk=profile_id)
        except Profile.DoesNotExist:
            raise Http404

    def get(self, request, profile_id):
        user_profile_list = Profile.objects.all().filter(id=profile_id)
        profile_serializer = UserProfileSerializer(user_profile_list, many=True)
        return Response(profile_serializer.data)

    def put(self, request, profile_id):
        model_object = self.get_object(profile_id)
        serializer = UserProfileSerializer(model_object, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class SystemTrackerAPI(APIView):
    def get(self, request, profile_id, format=None):
        serializer = SystemTrackerSerializer(SystemUsage.objects.filter(profile=profile_id), many=True)
        return Response(serializer.data)

    def post(self, request, profile_id, format=None):
        serializer = SystemTrackerSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class AttendanceAPI(APIView):
    """
    New API
    """
    def get(self, request, username, format=None):
        date = request.GET.get('date', False)
        serializer = AttendanceSerializer(Attendance.objects.filter(user=username, event_time__date=date).order_by('event_time'), many=True)
        return Response(serializer.data)

    def post(self, request, username, format=None):
        try:
            date = request.data['date']
            if date:
                serializer = AttendanceSerializer(Attendance.objects.filter(user=username, event_time__date=date).order_by('event_time'), many=True)
                for dt in serializer.data:
                    dt['event_time'] = datetime.datetime.strptime(dt['event_time'].replace('T', ' ').replace('Z', ''), "%Y-%m-%d %H:%M:%S").strftime("%H:%M:%S")
                return Response(serializer.data)
        except:
            serializer = AttendanceSerializer(data=request.data)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data, status=status.HTTP_201_CREATED)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class DailyWorkingHourAPI(APIView):
    """
    Daily Work hours api
    """
    def get(self, request, username, format=None):
        date = request.GET.get('date', False)
        month = request.GET.get('month', 'year').split("-")
        location = Employee.objects.get(employee_id=username).location
        default_days = 0
        if date:
            if str(location) == 'Chennai':
                try:
                    serializer = DailyWorkingHourSerializer(DailyWorkingHour.objects.get(user__username=username, date=date))
                except Exception as e:
                    try:
                        from scripts import mssql_connect
                        mssql_connect.get_data(username, date)
                        serializer = DailyWorkingHourSerializer(DailyWorkingHour.objects.get(user__username=username, date=date))
                    except Exception as e:
                        date_obj = date.split("-")
                        day = datetime.date(int(date_obj[0]), int(date_obj[1]), int(date_obj[2])).weekday()
                        if day == 6:
                            error_data = {"user": username, "date": date, "total": "Weekoff", "actual": "Weekoff",
                                          "non_completed": "Weekoff", "additional": "Weekoff"}
                        elif datetime.datetime.strptime(date, "%Y-%m-%d").date() < datetime.datetime.now().date():
                            error_data = {"user": username, "date": date, "total": "Absent", "actual": "Absent",
                                          "non_completed": "Absent", "additional": "Absent"}
                        else:
                            error_data = {"user": username, "date": date, "total": "No Data", "actual": "No Data", "non_completed": "No Data", "additional": "No Data"}
                        return Response(error_data)
            else:
                try:
                    serializer = DailyWorkingHourSerializer(DailyWorkingHour.objects.get(user__username=username, date=date))
                except:
                    date_obj = date.split("-")
                    day = datetime.date(int(date_obj[0]), int(date_obj[1]), int(date_obj[2])).weekday()
                    if day == 6:
                        error_data = {"user": username, "date": date, "total": "Weekoff", "actual": "Weekoff",
                                      "non_completed": "Weekoff", "additional": "Weekoff"}
                    elif datetime.datetime.strptime(date, "%Y-%m-%d").date() < datetime.datetime.now().date():
                        error_data = {"user": username, "date": date, "total": "Absent", "actual": "Absent",
                                      "non_completed": "Absent", "additional": "Absent"}
                    else:
                        error_data = {"user": username, "date": date, "total": "No Data", "actual": "No Data", "non_completed": "No Data", "additional": "No Data"}
                    return Response(error_data)
        else:
            serializer = DailyWorkingHourSerializer(DailyWorkingHour.objects.filter(user__username=username, date__month=month[1], date__year=month[0]), many=True)
            if str(location) == 'Kochi':
                default_days = AttendanceImport.objects.filter(upload_cochin__month=month[1], upload_cochin__year=month[0]).count()
            month_data = work_hours.get_month_info(serializer.data, default_days)
            return Response(month_data)
        return Response(serializer.data)

    def post(self, request, username, format=None):
        serializer = DailyWorkingHourSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class AttendanceImportsAPI(APIView):
    def get(self, request, format=None):
        serializer = AttendanceImportSerializer(AttendanceImport.objects.all(), many=True)
        return Response(serializer.data)

    def post(self, request):
        serializer = AttendanceImportSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
