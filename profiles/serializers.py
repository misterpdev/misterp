from rest_framework import serializers
from django.contrib.auth.models import User
from profiles.models import Profile, SystemUsage, Attendance, AttendanceImport, DailyWorkingHour


class UserProfileSerializer(serializers.ModelSerializer):
    password_changed = serializers.BooleanField(read_only=True)

    class Meta:
        model = Profile
        fields = '__all__'


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = '__all__'

    def create(self, validated_data):
        user = super(UserSerializer, self).create(validated_data)
        user.set_password(validated_data['password'])
        user.save()
        return user


class SystemTrackerSerializer(serializers.ModelSerializer):
    class Meta:
        model = SystemUsage
        fields = '__all__'


class AttendanceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Attendance
        fields = '__all__'

    def create(self, validated_data):
        io = super(AttendanceSerializer, self).create(validated_data)
        io.save()
        return io


class AttendanceImportSerializer(serializers.ModelSerializer):
    class Meta:
        model = AttendanceImport
        fields = '__all__'
        depth = 1


class DailyWorkingHourSerializer(serializers.ModelSerializer):
    user = serializers.SlugRelatedField(queryset=User.objects.all(), slug_field='username')

    class Meta:
        model = DailyWorkingHour
        fields = '__all__'
