from django.core.exceptions import ObjectDoesNotExist
from django.shortcuts import render, get_object_or_404
from django.contrib import auth
from django.http import HttpResponseRedirect
from django.contrib.auth.models import User
from .models import Profile,SystemUsage
from hrm.models import Employee


def login_view(request):
    """
    Login View
    :param request:
    :return:
    """
    if request.method == 'POST':
        username = request.POST.get('username', '')
        password = request.POST.get('password', '')
        user = auth.authenticate(username=username, password=password)
        if user is not None:
            auth.login(request, user)
            return HttpResponseRedirect('/')
        else:
            return render(request, 'profile/login.html', {'message': 'Invalid login credentials'})
    else:
        return render(request, 'profile/login.html')


def logout_view(request):
    """
    Logout View
    :param request:
    :return:
    """
    auth.logout(request)
    return HttpResponseRedirect('/')


def profile_view(request):
    """
    Profile details view
    :param request:
    :return:
    """
    context = {
        'self_profile': True,
        'user_profile': request.user,
    }
    return render(request, 'profile/profile.html', context=context)


def other_profile_view(request, user_id):
    """
    Other Profile details view
    :param request:
    :return:
    """
    user = get_object_or_404(User, id=user_id)
    context = {
        'self_profile': False,
        'user_profile': user,
    }
    return render(request, 'profile/profile.html', context=context)


def user_list(request):
    """
    Users List view
    :param request:
    :return:
    """
    return render(request, 'profile/user_list.html',
                  {'user_list': User.objects.all()})


def system_usage(request, user_id):
    """
    System usage view
    :param request:
    :param user_id:
    :return:
    """
    system = SystemUsage.objects.all()
    user = get_object_or_404(User, id=user_id)
    context = {
        'system': system,
        'user_profile': user,
    }
    return render(request, 'profile/system_usage.html', context=context)


def timeline(request, user_id):
    """
    Employee timeline view
    :param request:
    :param user_id:
    :return:
    """
    user = get_object_or_404(User, id=user_id)
    context = {
        'user_profile': user,
    }
    return render(request, 'profile/timeline.html', context=context)


def performance(request, user_id):
    """
    Employee performance view
    :param request:
    :param user_id:
    :return:
    """
    user = get_object_or_404(User, id=user_id)
    context = {
        'user_profile': user,
    }
    return render(request, 'profile/performance.html', context=context)
