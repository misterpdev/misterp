python manage.py dumpdata hrm > fixtures/hrm.json
python manage.py dumpdata production > fixtures/production.json
python manage.py dumpdata profiles > fixtures/profiles.json
python manage.py dumpdata auth > fixtures/auth.json