"""
This Script is used to import data in MIST ERP System
"""

import datetime
import csv
import pandas as pd
import requests

server = "http://127.0.0.1:8000"
token = '508aa703326d62a59156f21c531c33a4bf3e2128'
employee_csv = 'data/HR_MIST.csv'
attendance_csv = 'data/22.04.2019.xls'


########################################################################################################################

def calculate_hours(td):
    sec = td.total_seconds()
    hours = int(sec // 3600)
    minutes = int((sec % 3600) // 60)
    seconds = int(sec % 60)
    return "{}:{}:{}".format(str(hours).zfill(2), str(minutes).zfill(2), str(seconds).zfill(2))


def get_day_month_year(date):
    return date.day, date.month, date.year


def calculate_total(data):
    all_times = []
    total = datetime.timedelta()

    for d in data:
        all_times.append(d['event_datetime'])

    try:
        total = (max(all_times) - min(all_times))
    except:
        pass

    return total


def calculate_working_hours(data):
    for dt in data:
        dt['event_datetime'] = datetime.datetime.strptime((dt['event_time'].replace('T', ' ')).replace('Z', ''),
                                                          '%Y-%m-%d %H:%M:%S')

    working_hours = {'actual': datetime.timedelta(),
                     'total': datetime.timedelta(),
                     'non_completed': datetime.timedelta(),
                     'additional': datetime.timedelta()
                     }

    working_hours_str = {'actual': '',
                         'total': '',
                         'non_completed': '',
                         'additional': ''
                         }

    std_working_hours = datetime.timedelta(hours=8, minutes=30, seconds=00)
    i = 0

    while i < len(data):
        try:
            if data[i]['event_type'] == 'in' and data[i + 1]['event_point'] == data[i]['event_point'] and data[i + 1]['event_type'] == 'out':
                working_hours['actual'] += (data[i + 1]['event_datetime'] - data[i]['event_datetime'])
                i += 2
                continue
        except Exception as e:
            print(e)
            break

        i += 1

    if working_hours['actual'] < std_working_hours:
        working_hours['non_completed'] = std_working_hours - working_hours['actual']
    elif working_hours['actual'] > std_working_hours:
        working_hours['additional'] = working_hours['actual'] - std_working_hours

    working_hours['total'] = calculate_hours(calculate_total(data))

    working_hours_str['actual'] = calculate_hours(working_hours['actual'])
    working_hours_str['total'] = str(working_hours['total'])
    working_hours_str['non_completed'] = str(working_hours['non_completed'])
    working_hours_str['additional'] = str(working_hours['additional'])

    return working_hours_str


def calculate_artist_working_days(data):
    dates_list = []
    for dt in data:
        date = datetime.datetime.strptime((dt['event_time'].replace('T', ' ')).replace('Z', ''),
                                          '%Y-%m-%d %H:%M:%S').strftime('%Y-%m-%d')
        if date not in dates_list:
            dates_list.append(date)

    return len(dates_list)


########################################################################################################################


def calculate_working_hours_data(dates, ids):
    for date in dates:
        payload = {'date': date}
        for id in ids:
            attendance_api_url = "{}/api/attendance/{}/".format(server, id)
            response = requests.get(attendance_api_url, params=payload,
                                    headers={'Authorization': 'Token {}'.format(token)})
            daily_data = calculate_working_hours(response.json())
            daily_data['user'] = id
            daily_data['date'] = date
            daily_work_hours_url = "{}/api/work-hours/{}/".format(server, id)
            daily_data_response = requests.post(daily_work_hours_url, data=daily_data, headers={'Authorization': 'Token {}'.format(token)})
            print(daily_data_response.json())


def attendance_format_date(date):
    try:
        return datetime.datetime.strptime(date.replace('/', '-'), "%m-%d-%Y %H:%M:%S").strftime("%Y-%m-%d %H:%M:%S")
    except:
        return datetime.datetime.strptime(date.replace('/', '-'), "%m-%d-%Y %H:%M").strftime("%Y-%m-%d %H:%M:00")


def convert_date(date):
    try:
        return datetime.datetime.strptime(date.replace('T', ' ').replace('Z', ''), "%Y-%m-%d %H:%M:%S").strftime("%Y-%m-%d")
    except:
        return datetime.datetime.strptime(date.replace('T', ' ').replace('Z', ''), "%Y-%m-%d %H:%M").strftime("%Y-%m-%d")


def import_attendance_data():
    """
    This function will import attendance excel file data in ERP System
    :return:
    """
    response_dates = set()
    response_ids = set()

    df = pd.read_excel(attendance_csv, sheet_name='data')

    for i in df.index:
        if str(df['Postcode'][i]) != 'nan':
            user_id = "M{}".format(int(df['Postcode'][i]))
        else:
            continue
        if user_id in ['M2044', 'M2045', 'M2054', 'M2065', 'M2064', 'M2063', 'M2062', 'M']:
            continue
        reader_name = df['Reader Name'][i]
        date_time = df['Time'][i]
        date_time_str = "{} {}".format(date_time.date(), date_time.time())
        door = df['Event Point'][i]
        event = df['Event Description'][i].replace(' ', '')
        io = reader_name.split(":")[1].replace(' ', '').lower()

        data = {
            "user": user_id,
            "event_time": date_time_str,
            "event_type": io,
            "event_point": door,
            "event_description": event,
        }
        user_api_url = "{}/api/attendance/{}/?date={}".format(server, user_id, date_time.date())
        response = requests.post(user_api_url, data=data, headers={'Authorization': 'Token {}'.format(token)})
        response_json = response.json()
        print(response_json)
        response_dates.add(convert_date(response_json['event_time']))
        response_ids.add(response_json['user'])

    imports_api = "{}/api/attendance-imports/".format(server)
    for date in list(response_dates):
        print(date)
        import_response = requests.post(imports_api, data={"upload_cochin": date}, headers={'Authorization': 'Token {}'.format(token)})
        print(import_response.json())
    calculate_working_hours_data(list(response_dates), list(response_ids))


def employee_format_date(date_value):
    return datetime.datetime.strptime(date_value, "%d.%m.%Y").strftime('%Y-%m-%d')


def update_employee_details(data, row):
    """
    This function will read data from employee csv file and update in hrm
    employee model
    :param data:
    :param row:
    :return:
    """
    user_api_url = "{}/api/hrm/employee/{}/".format(server, data['id'])
    data_edit = {
        "employee_id": row['EMPLOYEE ID'],
        "full_name": row['FULL NAME'],
        "first_name": row['FIRST NAME'],
        "last_name": row['LAST NAME'],
        "date_of_birth": employee_format_date(row['DATE OF BIRTH']),
        "gender": row['GENDER'],
        "marital_status": row['MARITAL STATUS'],
        "address_1": row['ADDRESS 1'],
        "address_2": row['ADDRESS 2'],
        "address_city": row['ADDRESS CITY'],
        "address_state": row['ADDRESS STATE'],
        "address_zip": row['ADDRESS ZIP'],
        "address_country": row['ADDRESS COUNTRY'],
        "work_email": row['WORK EMAIL'],
        "other_email": row['OTHER EMAIL'],
        "phone": row['PHONE'],
        "joining_date": employee_format_date(row['JOINING DATE']),
        "employment_status": row['EMPLOYMENT STATUS'],
        "department": row['DEPARTMENT'],
        "designation": row['DESIGNATION'],
        "location": row['LOCATION'],

    }
    requests.put(user_api_url, data=data_edit, headers={'Authorization': 'Token {}'.format(token)})

    init_leaves_entitlement = {
        "start_date": "2019-01-01",
        "end_date": "2019-12-31",
        "el_leaves": 10.0,
        "cl_leaves": 5.0,
        "employee": data['id']
    }
    entitlement_url = "{}/api/hrm/leaves/entitlement/".format(server)
    r = requests.post(entitlement_url, data=init_leaves_entitlement, headers={'Authorization': 'Token {}'.format(token)})
    print(r.text)


def import_employee_data():
    """
    Import employee data from csv file
    :return:
    """
    default_password = "mist1234"
    user_api_url = "{}/api/users/".format(server)

    with open(employee_csv, newline='') as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            print(row['EMPLOYEE ID'], row['FIRST NAME'], row['LAST NAME'])
            data = {
                "id": 1,
                "password": default_password,
                "is_superuser": False,
                "username": row['EMPLOYEE ID'],
                "first_name": row['FIRST NAME'],
                "last_name": row['LAST NAME'],
                "email": row['WORK EMAIL'],
                "is_staff": False,
                "is_active": True
            }
            response = requests.post(user_api_url, data=data, headers={'Authorization': 'Token {}'.format(token)})
            print(response.text)
            update_employee_details(response.json(), row)


def generate_default_profile_photo():
    import shutil
    with open(employee_csv, newline='') as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            print(row['EMPLOYEE ID'], row['GENDER'])
            if row['GENDER'] == 'male':
                shutil.copyfile('img/male.png', 'img/default/{}.jpg'.format(row['EMPLOYEE ID']))
            elif row['GENDER'] == 'female':
                shutil.copyfile('img/female.png', 'img/default/{}.jpg'.format(row['EMPLOYEE ID']))


if __name__ == '__main__':
    #generate_default_profile_photo()
    import_attendance_data()
