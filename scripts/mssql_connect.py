
import pymssql
import requests
import datetime

########################################################################################################################

def calculate_hours(td):
    sec = td.total_seconds()
    hours = int(sec // 3600)
    minutes = int((sec % 3600) // 60)
    seconds = int(sec % 60)
    return "{}:{}".format(str(hours).zfill(2), str(minutes).zfill(2))

def get_day_month_year(date):
    return date.day, date.month, date.year


def calculate_total(data):
    all_times = []
    total = datetime.timedelta()

    for d in data:
        all_times.append(d['event_datetime'])

    try:
        total = (max(all_times) - min(all_times))
    except:
        pass

    return total

def calculate_working_hours(data):
    for dt in data:
        datetime_str = datetime.datetime.strptime(dt['event_time'], '%Y-%m-%d %H:%M:%S').strftime("%H:%M")
        dt['event_datetime'] = datetime.datetime.strptime(datetime_str, "%H:%M")

    working_hours = {'actual': datetime.timedelta(),
                     'total': datetime.timedelta(),
                     'non_completed': datetime.timedelta(),
                     'additional': datetime.timedelta()
                     }

    working_hours_str = {'actual': '',
                         'total': '',
                         'non_completed': '',
                         'additional': ''
                         }

    std_working_hours = datetime.timedelta(hours=8, minutes=30, seconds=00)
    i = 0

    while i < len(data):
        try:
            if data[i]['event_type'] == 'in' and data[i + 1]['event_point'] == data[i]['event_point'] and data[i + 1]['event_type'] == 'out':
                working_hours['actual'] += (data[i + 1]['event_datetime'] - data[i]['event_datetime'])
                try:
                    working_hours['actual'] = working_hours['actual'] - datetime.timedelta(days=working_hours['actual'].days)
                except Exception as e:
                    pass
                i += 2
                continue
        except Exception as e:
            print(e)
            break

        i += 1

    if working_hours['actual'] < std_working_hours:
        working_hours['non_completed'] = std_working_hours - working_hours['actual']
    elif working_hours['actual'] > std_working_hours:
        working_hours['additional'] = working_hours['actual'] - std_working_hours

    working_hours['total'] = calculate_hours(calculate_total(data))

    working_hours_str['actual'] = calculate_hours(working_hours['actual'])
    working_hours_str['total'] = str(working_hours['total'])
    working_hours_str['non_completed'] = str(working_hours['non_completed'])
    working_hours_str['additional'] = str(working_hours['additional'])

    return working_hours_str

########################################################################################################################

server = "http://127.0.0.1:8000"
token = '508aa703326d62a59156f21c531c33a4bf3e2128'

try:
    conn = pymssql.connect("192.168.1.222:11706", "sa", "Mist@123", "NEWDB", autocommit=True, login_timeout=1)
except Exception as e:
    print(e)


def get_empid(id):
    cursor1 = conn.cursor()
    cursor1.execute("SELECT EmployeeId FROM Employees WHERE EmployeeCode = '{}'".format(id))
    return (cursor1.fetchone()[0])


def get_data(username, date):
    cursor = conn.cursor()
    id = get_empid(username)
    att_data = []
    """ AttendanceDate , EmployeeId, PunchRecords """
    cursor.execute("SELECT PunchRecords, AttendanceDate FROM AttendanceLogs WHERE EmployeeId = '{}' AND CAST(AttendanceDate as Date) = '{}'".format(id, date))
    for data in cursor.fetchall():
        var = data[0].split(",")
        for p in var:
            if p == '':
                continue
            a = p.split(":")
            time = ":".join([a[0], a[1]])
            date_time = "{} {}:00".format(date, time)
            io = a[2].split("(")[0]
            door = a[2].split("(")[1].replace(')', '')
            event = "Normal Open"

            attendance_data = {
                "event_time": date_time,
                "event_type": io,
                "event_point": door,
                "profile": id,
                "user": username,
                "event_description": event
            }
            att_data.append(attendance_data)
            attendance_api_url = "{}/api/attendance/{}/".format(server, username)
            response = requests.post(attendance_api_url, data=attendance_data, headers={'Authorization': 'Token {}'.format(token)})

    ret_data = calculate_working_hours(att_data)
    ret_data['user'] = username
    ret_data['date'] = date

    if ret_data['actual'] != '00:00':
        daily_work_hours_url = "{}/api/work-hours/{}/".format(server, id)
        daily_data_response = requests.post(daily_work_hours_url, data=ret_data, headers={'Authorization': 'Token {}'.format(token)})

    #conn.close()

if __name__ == '__main__':
    #check_data()
    get_data('M1011', '2019-04-05')
