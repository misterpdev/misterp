import datetime

def calculate_hours(td):
    sec = td.total_seconds()
    hours = int(sec // 3600)
    minutes = int((sec % 3600) // 60)
    seconds = int(sec % 60)
    return "{}:{}:{}".format(str(hours).zfill(2), str(minutes).zfill(2), str(seconds).zfill(2))

def get_day_month_year(date):
    return date.day, date.month, date.year


def calculate_total(data):
    all_times = []
    total = datetime.timedelta()

    for d in data:
        all_times.append(d['event_datetime'])

    try:
        total = (max(all_times) - min(all_times))
    except:
        pass

    return total


def calculate_working_hours(data):
    for dt in data:
        dt['event_datetime'] = datetime.datetime.strptime((dt['event_time'].replace('T', ' ')).replace('Z', ''),
                                                          '%Y-%m-%d %H:%M:%S')

    working_hours = {'actual': datetime.timedelta(),
                     'total': datetime.timedelta(),
                     'non_completed': datetime.timedelta(),
                     'additional': datetime.timedelta()
                     }

    working_hours_str = {'actual': '',
                         'total': '',
                         'non_completed': '',
                         'additional': ''
                         }

    std_working_hours = datetime.timedelta(hours=8, minutes=30, seconds=00)
    i = 0

    while i < len(data):
        try:
            if data[i]['event_type'] == 'in' and data[i + 1]['event_point'] == data[i]['event_point'] and data[i + 1]['event_type'] == 'out':
                working_hours['actual'] += (data[i + 1]['event_datetime'] - data[i]['event_datetime'])
                i += 2
                continue
        except Exception as e:
            print(e)
            break

        i += 1

    if working_hours['actual'] < std_working_hours:
        working_hours['non_completed'] = std_working_hours - working_hours['actual']
    elif working_hours['actual'] > std_working_hours:
        working_hours['additional'] = working_hours['actual'] - std_working_hours

    working_hours['total'] = calculate_hours(calculate_total(data))

    working_hours_str['actual'] = calculate_hours(working_hours['actual'])
    working_hours_str['total'] = str(working_hours['total'])
    working_hours_str['non_completed'] = str(working_hours['non_completed'])
    working_hours_str['additional'] = str(working_hours['additional'])

    return working_hours_str


def calculate_artist_working_days(data):
    dates_list = []
    for dt in data:
        date = datetime.datetime.strptime((dt['event_time'].replace('T', ' ')).replace('Z', ''),
                                          '%Y-%m-%d %H:%M:%S').strftime('%Y-%m-%d')
        if date not in dates_list:
            dates_list.append(date)

    return len(dates_list)


"""def calculate_month_working_hours(data, office_working_days=7):
    # need a method to get office_working_days after reducing leave
    artist_working_days = calculate_artist_working_days(data)
    current_data = calculate_working_hours(data)
    total_current = current_data['total'].split(":")
    total_current_td = datetime.timedelta(hours=int(total_current[0]), minutes=int(total_current[1]),
                                          seconds=int(total_current[2]))
    actual_current = current_data['actual'].split(":")
    actual_current_td = datetime.timedelta(hours=int(actual_current[0]), minutes=int(actual_current[1]),
                                           seconds=int(actual_current[2]))

    month_info = {
        'total': {'default': datetime.timedelta(), 'target': datetime.timedelta(), 'current': datetime.timedelta(),
                  'incomplete': datetime.timedelta()},
        'actual': {'default': datetime.timedelta(), 'target': datetime.timedelta(), 'current': datetime.timedelta(),
                   'incomplete': datetime.timedelta()},
        'days': {'default': datetime.timedelta(), 'target': datetime.timedelta(), 'current': '-',
                 'incomplete': datetime.timedelta()}
    }

    month_info_str = {
        'total': {'default': '00:00:00', 'target': '00:00:00', 'current': '00:00:00', 'incomplete': '00:00:00'},
        'actual': {'default': '00:00:00', 'target': '00:00:00', 'current': '00:00:00', 'incomplete': '00:00:00'},
        'days': {'default': '00:00:00', 'target': '00:00:00', 'current': '00:00:00', 'incomplete': '00:00:00'}
    }

    month_info_str['days']['default'] = str(office_working_days)
    month_info_str['days']['target'] = str(artist_working_days)
    month_info_str['days']['incomplete'] = str(office_working_days - artist_working_days)

    month_info['total']['default'] = datetime.timedelta(hours=10, minutes=00, seconds=00) * office_working_days
    month_info_str['total']['default'] = calculate_hours(
        datetime.timedelta(hours=10, minutes=00, seconds=00) * office_working_days)
    month_info['total']['target'] = datetime.timedelta(hours=10, minutes=00, seconds=00) * artist_working_days
    month_info_str['total']['target'] = calculate_hours(
        datetime.timedelta(hours=10, minutes=00, seconds=00) * artist_working_days)
    month_info_str['total']['current'] = current_data['total']
    if month_info['total']['target'] > total_current_td:
        month_info['total']['incomplete'] = month_info['total']['target'] - total_current_td
        month_info_str['total']['incomplete'] = calculate_hours(month_info['total']['target'] - total_current_td)

    month_info['actual']['default'] = datetime.timedelta(hours=8, minutes=30, seconds=00) * office_working_days
    month_info_str['actual']['default'] = calculate_hours(
        datetime.timedelta(hours=8, minutes=30, seconds=00) * office_working_days)
    month_info['actual']['target'] = datetime.timedelta(hours=8, minutes=30, seconds=00) * artist_working_days
    month_info_str['actual']['target'] = calculate_hours(
        datetime.timedelta(hours=8, minutes=30, seconds=00) * artist_working_days)
    month_info_str['actual']['current'] = current_data['actual']
    if month_info['actual']['target'] > actual_current_td:
        month_info['actual']['incomplete'] = month_info['actual']['target'] - actual_current_td
        month_info_str['actual']['incomplete'] = calculate_hours(month_info['actual']['target'] - actual_current_td)

    return month_info_str"""

def get_month_info(data, default_days):
    artist_days = len(data)
    month_data = {}
    month_data['total'] = {'default': '00:00:00',
                           'target': '00:00:00',
                           'current': '00:00:00',
                           'incomplete': '00:00:00'}
    month_data['actual'] = {'default': '00:00:00',
                            'target': '00:00:00',
                            'current': '00:00:00',
                            'incomplete': '00:00:00'}
    month_data['days'] = {'default': '0',
                          'target': '0',
                          'current': '-',
                          'incomplete': '0'}
    if data == []:
        return month_data
    total = datetime.timedelta(); actual = datetime.timedelta(); non_completed = datetime.timedelta(); additional = datetime.timedelta()
    for dt in data:
        t = list(map(int, dt['total'].split(":")))
        total += datetime.timedelta(hours=t[0], minutes=t[1], seconds=t[2])
        a = list(map(int, dt['actual'].split(':')))
        actual += datetime.timedelta(hours=a[0], minutes=a[1], seconds=a[2])
        nc = list(map(int, dt['non_completed'].split(':')))
        non_completed += datetime.timedelta(hours=nc[0], minutes=nc[1], seconds=nc[2])
        ad = list(map(int, dt['additional'].split(':')))
        additional += datetime.timedelta(hours=ad[0], minutes=ad[1], seconds=ad[2])

    month_data['total']['default'] = calculate_hours(datetime.timedelta(hours=10, minutes=00, seconds=00) * default_days)
    month_data['total']['target'] = calculate_hours(datetime.timedelta(hours=10, minutes=00, seconds=00) * artist_days)
    month_data['total']['current'] = calculate_hours(total)
    incomplete = datetime.timedelta(hours=10, minutes=00, seconds=00) * artist_days - total
    month_data['total']['incomplete'] = calculate_hours(incomplete if incomplete > datetime.timedelta() else datetime.timedelta())

    month_data['actual']['default'] = calculate_hours(datetime.timedelta(hours=8, minutes=30, seconds=00) * default_days)
    month_data['actual']['target'] = calculate_hours(datetime.timedelta(hours=8, minutes=30, seconds=00) * artist_days)
    month_data['actual']['current'] = calculate_hours(actual)
    month_data['actual']['incomplete'] = calculate_hours(non_completed)

    month_data['days']['default'] = str(default_days)
    month_data['days']['target'] = str(artist_days)
    month_data['days']['incomplete'] = str(default_days-artist_days)

    return month_data