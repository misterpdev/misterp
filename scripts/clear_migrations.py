import os

os.remove('db.sqlite3')

paths = [
         './common/migrations',
         './hrm/migrations',
         './production/migrations',
         './inventory/migrations',
         './profiles/migrations',
        ]

for path in paths:
    files = (os.listdir(path))
    for f in files:
        if not f.startswith('__'):
            file_path = os.path.join(path, f)
            os.remove(file_path)
            print('Removed : {}'.format(file_path))
