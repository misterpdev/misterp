// InitializeAttachmentTable
var attachment_table = $('#attachments_datatable').dataTable({
"searching": false,
"paging": false,
"lengthMenu": [25, 25, 25, 25, 25],
ajax: {
    url: "/api/hrm/attachments/" + 0 + "/",
    dataSrc: ''
},
"columnDefs": [
    {"title": "File", "targets": 0},
    {"title": "File Name", "targets": 1},
    {"title": "Type", "targets": 2},
    {"title": "Uploaded Date", "targets": 3},
    {"title": "Remove File", "targets": 4},
],
columns: [
    {
        "data": "file",
        "render": function (data, type, row, meta) {
            if (type === 'display') {
                data = '<a href="' + data + '" data-fancybox="gallery" ><img src="' + data + ' " onerror=this.src="/media/profiles/photo/pdf.png"  alt="files" width="70" height="70"  /></a>';
            }
            return data;
        }
    },
    {
        "data": "file",
        "render": function (data, type, row, meta) {
            if (type === 'display') {
                var name = data.split("/")[4];
                data = '<p> ' + name + ' </p>';
            }
            return data;
        }
    },
    {"data": "attachment_type"},
    {"data": "creation_date"},
    {
        "data": "id",
        "render": function (data, type, row, meta) {
            if (type === 'display') {
                data = '<button class="btn btn-danger" onclick="delAttach(' + data + ')" >Delete</button>';
            }
            return data;
        }
    },
]
});


// Render Employee Detail
function render_employee_detail(employee, disabled = false) {
    if (disabled) {
        var fieldset = "<fieldset disabled>"
    } else {
        var fieldset = "<fieldset>"
    }

    var department = 'Department';
    if (employee.department) {
        var department = employee.department;
    }
    var designation = 'Designation';
    if (employee.designation) {
        var designation = employee.designation.name;
    }
    var location = 'Location';
    if (employee.location) {
        var location = employee.location.name;
    }
    var role = 'Role';
    if (employee.role) {
        var role = employee.role.name;
    }
    var employee_status = 'EmploymentStatus';
    if (employee.employment_status) {
        var employee_status = employee.employment_status.name;
    }

    $('#Employee_Card_Modal').empty();
    var employee_detail_div = `
            <div class="card">
              <div class="card-body">
              <form id="upload_file" class="form-material" role="form" enctype="multipart/form-data">
              ${fieldset}
                <center class="mt-4"> <img id="profile_pic"  src="${employee.photo}" onerror="this.src='/media/profiles/photo/default.png'" class="img-circle" width="150" />
                    <input type="file" id="ProfilePictureUpload" style="display: none" />
                    <h4 class="card-title mt-2">${employee.full_name}</h4>
                    <h6 class="card-subtitle">${employee.department}</h6>
                    <h6 class="card-subtitle">${employee.location}</h6>
                </center>
                </fieldset>
               </form>

              </div>

              <div>
                  <hr> </div>
                  <div class="card-body"> <small class="text-muted">Email address </small>
                    <h6>${employee.work_email}</h6> <small class="text-muted p-t-30 db">Phone</small>
                    <h6>+91-${employee.phone}</h6> <small class="text-muted p-t-30 db">Address</small>
                    <h6>${employee.address_1},</h6><h6>${employee.address_city},${employee.address_state},</h6><h6>${employee.address_zip}</h6>

              </div>
            </div>
    `;
    $('#Employee_Card_Modal').append(employee_detail_div);

//Render Employee Details General tab
    $('#general').empty();
    var employee_detail_general_div = `
      <div class="card-body">
        <form id="update_general" class="form-horizontal form-material">
         ${fieldset}
          <div class="form-group">
            <div class="row">

              <div class="col-md-4">
                <label class="col-md-6" style="margin-left:-14px;">Full Name</label>
                <input id="general_full_name" type="text" name="text" value="${employee.full_name}" class="form-control form-control-line">
                </input>
              </div>

              <div class="col-md-4">
                <label class="col-md-6" style="margin-left:-14px;">First Name</label>
                <input id="general_first_name" type="text" name="text" value="${employee.first_name}" class="form-control form-control-line">
                </input>
              </div>
              <div class="col-md-4">
                <label class="col-md-6" style="margin-left:-14px;">Last Name</label>
                <input id="general_last_name" type="text" name="text" value="${employee.last_name}" class="form-control form-control-line">
                </input>
              </div>
            </div>
          </div>
          <div class="form-group">
            <label for="email" class="col-md-12">Email</label>
            <div class="col-md-12">
              <input id="general_email" type="email" value="${employee.work_email}" class="form-control form-control-line" name="example-email" id="example-email">
            </div>
          </div>
          <div class="form-group">
            <label class="col-md-12">Date of Birth</label>
            <div class="col-md-12">
              <input id="general_dob" type="date" value="${employee.date_of_birth}" class="form-control form-control-line">
            </div>
          </div>
          <div class="form-group" style="margin-left:15px;">
            <div class="row">
              <div class="col-md-6">
                <label class="col-md-6" style="margin-left:-14px;">Phone</label>
                <input id="general_phone" type="text" value="${employee.phone}" class="form-control form-control-line">
              </div>
              <div class="col-md-6">
                <label class="col-md-6" style="margin-left:-14px;">Alternate Phone</label>
                <input id="general_alt_phone" type="text" value="${employee.alt_phone}" class="form-control form-control-line">
              </div>
            </div>
          </div>
          <div class="form-group" style="margin-left:15px;">
            <div class="row">
              <div class="col-sm-6">
              <label class="col-md-6" style="margin-left:-14px;">Gender</label>
                <select id="general_gender" class="form-control form-control-line">
                    <option value="male">Male</option>
                    <option value="female">Female</option>
                </select>
              </div>
              <div class="col-sm-6">
              <label class="col-md-6" style="margin-left:-14px;">Martial Status</label>
                <select id="general_marital" value="${employee.marital_status}" class="form-control form-control-line">
                    <option value="single">Single</option>
                    <option value="married">Married</option>
                </select>
              </div>
            </div>
          </div>
          <div class="form-group" style="margin-left:15px;">
            <div class="row">
              <div class="col-sm-6">
                <label class="col-md-6" style="margin-left:-14px;">Blood Group</label>
                <input id="general_blood" type="text" value="${employee.blood_group}" class="form-control form-control-line">
              </div>
              <div class="col-sm-6">
              <label class="col-md-6" style="margin-left:-14px;">PAN NUMBER</label>
                <input id="general_pan" type="text" value="${employee.pan_number}" class="form-control form-control-line">
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="col-sm-12">
              <button type="submit" onclick="updateGeneral(${employee.id})" class="btn btn-success">Update Profile</button>
            </div>
          </div>
           </fieldset>
        </form>
      </div>
    `;
    $('#general').append(employee_detail_general_div);
    $('#general_gender').val(employee.gender);
    $('#general_marital').val(employee.marital_status);


//Render Employee Details Contact tab
    $('#contact').empty();
    var employee_detail_contact_div = `
    <div class="card-body">
      <form id ="update_contact" class="form-horizontal form-material" >
        ${fieldset}
        <h4 class="box-title">Residential Address</h4>
        <hr class="mt-0 mb-3">
        <div class="form-group">
          <div class="row">
            <div class="col-md-6">
            <label class="col-md-6" style="margin-left:-14px;">Address_1</label>
              <input type="text" id="contact_address_1" value="${employee.address_1}" class="form-control form-control-line">
            </div>
            <div class="col-md-6">
              <label class="col-md-6" style="margin-left:-14px;">Address_2</label>
              <input type="text" id="contact_address_2" value="${employee.address_2}" class="form-control form-control-line">
            </div>
          </div>
        </div>
        <div class="form-group">
          <div class="row">
            <div class="col-md-6">
              <label class="col-md-6" style="margin-left:-14px;">City</label>
              <input type="text" id="contact_address_city" value="${employee.address_city}" class="form-control form-control-line">
            </div>
            <div class="col-md-6">
              <label class="col-md-6" style="margin-left:-14px;">State</label>
              <input type="text" id="contact_address_state" value="${employee.address_state}" class="form-control form-control-line">
            </div>
          </div>
        </div>
        <div class="form-group">
          <div class="row">
            <div class="col-md-6">
              <label class="col-md-6" style="margin-left:-14px;">Zip</label>
              <input type="number" id="contact_address_zip" value="${employee.address_zip}" class="form-control form-control-line">
            </div>
            <div class="col-md-6">
              <label class="col-md-6" style="margin-left:-14px;">Country</label>
              <input type="text" id="contact_address_country" value="${employee.address_country}" class="form-control form-control-line">
            </div>
          </div>
        </div>
        <h4 class="box-title">Correspondence Address</h4>
        <hr class="mt-0 mb-2">
        <div class="form-group">
          <div class="row">
            <div class="col-md-6">
            <label class="col-md-6" style="margin-left:-14px;">Address_1</label>
              <input type="text" id="contact_c_address_1" value="${employee.c_address_1}" class="form-control form-control-line">
            </div>
            <div class="col-md-6">
              <label class="col-md-6" style="margin-left:-14px;">Address_2</label>
              <input type="text" id="contact_c_address_2" value="${employee.c_address_2}" class="form-control form-control-line">
            </div>
          </div>
        </div>
        <div class="form-group">
          <div class="row">
            <div class="col-md-6">
              <label class="col-md-6" style="margin-left:-14px;">City</label>
              <input type="text" id="contact_c_address_city" value="${employee.c_address_city}" class="form-control form-control-line">
            </div>
            <div class="col-md-6">
              <label class="col-md-6" style="margin-left:-14px;">State</label>
              <input type="text" id="contact_c_address_state" value="${employee.c_address_state}" class="form-control form-control-line">
            </div>
          </div>
        </div>
        <div class="form-group">
          <div class="row">
            <div class="col-md-6">
              <label class="col-md-6" style="margin-left:-14px;">Zip</label>
              <input type="number" id="contact_c_address_zip" value="${employee.c_address_zip}" class="form-control form-control-line">
            </div>
            <div class="col-md-6">
              <label class="col-md-6" style="margin-left:-14px;">Country</label>
              <input type="text" id="contact_c_address_country" value="${employee.c_address_country}" class="form-control form-control-line">
            </div>
          </div>
        </div>
        <div class="form-group">
          <div class="col-sm-12">
            <button class="btn btn-success" type="submit" onclick="updateContact(${employee.id})">Update Profile</button>
          </div>
        </div>
        </fieldset>
      </form>
    </div>
    `;
    $('#contact').append(employee_detail_contact_div);


//Render Employee Details Job tab
    var designation_values = [];
    $.each(designations, function (key, value) {
        designation_values.push('<option value="' + value + '">' + value + '</option>');
    });
    var department_values = [];
    $.each(departments, function (key, value) {
        department_values.push('<option value="' + value + '">' + value + '</option>');
    });
    var role_values = [];
    $.each(roles, function (key, value) {
        role_values.push('<option value="' + value + '">' + value + '</option>');
    });
    var location_values = [];
    $.each(locations, function (key, value) {
        location_values.push('<option value="' + value + '">' + value + '</option>');
    });
    var employment_status_values = [];
    $.each(employment_statuss, function (key, value) {
        employment_status_values.push('<option value="' + value + '">' + value + '</option>');
    });

    $('#job').empty();
    var employee_detail_job_div = `
    <div class="card-body">
      <form id = "update_job" class="form-horizontal form-material" >
        ${fieldset}
        <div class="form-group">
          <label class="col-md-12">Employee Id</label>
          <div class="col-md-12">
            <input type="text" id="job_employee_id" value="${employee.employee_id}"" class="form-control form-control-line">
          </div>
        </div>
        <div class="form-group">
          <label class="col-md-12">Date of Joining</label>
          <div class="col-md-12">
            <input id="job_doj" type="date" value="${employee.joining_date}" class="form-control form-control-line">
          </div>
        </div>
        <div class="form-group">
          <label class="col-md-12">Designation</label>
          <div class="col-md-12">
            <select class="form-control" id="job_designation">
              {% for e in ${designation_values} %}
              {{ e }}
              {% endfor %}
            </select>
          </div>
        </div>
        <div class="form-group">
          <label class="col-md-12">Role</label>
          <div class="col-md-12">
            <select class="form-control" id="job_role">
              {% for e in ${role_values} %}
              {{ e }}
              {% endfor %}
            </select>
          </div>
        </div>
        <div class="form-group">
          <label class="col-md-12">Department</label>
          <div class="col-md-12">
            <select class="form-control" id="job_department">
              {% for e in ${department_values} %}
              {{ e }}
              {% endfor %}
            </select>
          </div>
        </div>
        <div class="form-group">
          <label class="col-md-12">Employment Status</label>
          <div class="col-md-12">
            <select class="form-control" id="job_employement_status">
              {% for e in ${employment_status_values} %}
              {{ e }}
              {% endfor %}
            </select>
          </div>
        </div>
        <div class="form-group">
            <label class="col-md-12">Date of Resigned</label>
            <div class="col-md-12">
              <input id="job_dor" type="date" value="${employee.resign_date}" class="form-control form-control-line">
            </div>
        </div>

        <div class="form-group">
          <label class="col-md-12">Location</label>
          <div class="col-md-12">
            <select class="form-control" id="job_location">
              {% for e in ${location_values} %}
              {{ e }}
              {% endfor %}
            </select>
          </div>
        </div>
        <div class="form-group">
          <div class="col-sm-12">
            <button class="btn btn-success" type="submit" onclick="updateJob(${employee.id})" >Update Profile</button>
          </div>
        </div>
        </fieldset>
      </form>
    </div>
    `;

    $('#job').append(employee_detail_job_div);
    $('#job_location').val(employee.location);
    $('#job_department').val(employee.department);
    $('#job_role').val(employee.role);
    $('#job_designation').val(employee.designation);
    $('#job_employement_status').val(employee.employment_status);

    //Render Bank Details Job tab

      $('#bank').empty();
      var employee_detail_bank_div = `
      <div class="card-body">
        <form id = "update_bank" class="form-horizontal form-material" >
          ${fieldset}
          <div class="form-group">
            <label class="col-md-12">Bank Name</label>
            <div class="col-md-12">
              <input type="text" id="bank_name" value="${employee.bank_name}"" class="form-control form-control-line">
            </div>
          </div>
          <div class="form-group">
            <label class="col-md-12">Account Number</label>
            <div class="col-md-12">
              <input id="bank_account" type="text" value="${employee.account_number}" class="form-control form-control-line">
            </div>
          </div>
          <div class="form-group">
            <label class="col-md-12">IFSC Code</label>
            <div class="col-md-12">
              <input id="bank_ifsc" type="text" value="${employee.ifsc_code}" class="form-control form-control-line">
            </div>
          </div>
          <div class="form-group">
            <label class="col-md-12">Branch</label>
            <div class="col-md-12">
              <input id="bank_branch" type="text" value="${employee.bank_branch}" class="form-control form-control-line">
            </div>
          </div>
          <div class="form-group">
            <div class="col-sm-12">
              <button class="btn btn-success" type="submit" onclick="updateBank(${employee.id})" >Update Profile</button>
            </div>
          </div>
          </fieldset>
        </form>
      </div>
      `;
      $('#bank').append(employee_detail_bank_div);
//Render Employee Details Entitlement tab
    var employee_entitle_json = $.ajax({
        url: "/api/hrm/leaves/entitlement/" + employee.id + '/',
        type: "GET",
        dataType: "application/json",
        async: false,
        error: function (xhr, status, errorThrown) {
        }
    }).responseText;
    var entitle = JSON.parse(employee_entitle_json);
    var employee_lop_json = $.ajax({
        url: "/api/hrm/leaves/employee/" + employee.id + '/',
        type: "GET",
        dataType: "application/json",
        async: false,
        error: function (xhr, status, errorThrown) {
        }
    }).responseText;
    var lop = JSON.parse(employee_lop_json);
    var lops = 0;
    for (i in lop) {
        lops += lop[i]['lop_days'];
    }
    var tot = 0;
    for (j in lop) {
        tot += lop[j]['cl_days'] + lop[j]['el_days'] + lop[j]['lop_days']
    }
    var date = new Date();
    var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
    var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);

    var lastDayWithSlashes = lastDay.getFullYear() + '-' + (lastDay.getMonth() + 1) + '-' + (lastDay.getDate());
    var firstDayWithSlashes = firstDay.getFullYear() + '-' + (firstDay.getMonth() + 1) + '-' + (firstDay.getDate());

    if (entitle.start_date >= firstDayWithSlashes) {

    }

    $('#entitle').empty();
    var employee_detail_entitle_div = `
    <div class="card-body">
      <form id = "update_entitle" class="form-horizontal form-material" role="form" >
            ${fieldset}
           <h3 class="box-title">Entitlement Info</h3>
           <hr class="mt-0 mb-5">
           <div class="row">
             <div class="col-lg-6 col-md-12">
                <div class="form-group row">
                    <label class="control-label text-right col-md-8">Remaining Casual Leaves :</label>
                    <div class="col-md-3">
                        <span class="label label-rounded label-success">${entitle.cl_leaves}</span>
                    </div>
                </div>
             </div>
           </div>
           <div class="row">
             <div class="col-lg-6 col-md-12">
                <div class="form-group row">
                    <label class="control-label text-right col-md-8">Remaining Earned Leaves :</label>
                    <div class="col-md-3">
                        <span class="label label-rounded label-success">${entitle.el_leaves}</span>
                    </div>
                </div>
             </div>
           </div>
           <div class="row">
             <div class="col-lg-6 col-md-12">
                <div class="form-group row">
                    <label class="control-label text-right col-md-8">Total LOP's in this Month:</label>
                    <div class="col-md-3">
                        <span class="label label-rounded label-success">${lops}</span>
                    </div>
                </div>
             </div>
           </div>
           <div class="row">
             <div class="col-lg-6 col-md-12">
                <div class="form-group row">
                    <label class="control-label text-right col-md-8">Total LOP's in this Year:</label>
                    <div class="col-md-3">
                          <span class="label label-rounded label-success">${lops}</span>
                    </div>
                </div>
             </div>
           </div>
           <div class="row">
             <div class="col-lg-6 col-md-12">
                <div class="form-group row">
                    <label class="control-label text-right col-md-8">Total taken Leaves in this Month:</label>
                    <div class="col-md-3">
                        <span class="label label-rounded label-success">${tot}</span>
                    </div>
                </div>
             </div>
           </div>
           <div class="row">
             <div class="col-lg-6 col-md-12">
                <div class="form-group row">
                    <label class="control-label text-right col-md-8">Total taken Leaves in this Year:</label>
                    <div class="col-md-3">
                        <span class="label label-rounded label-success">${tot}</span>
                    </div>
                </div>
             </div>
           </div>
           </fieldset>
      </form>
    </div>
    `;
    $('#entitle').append(employee_detail_entitle_div);


// Render upload Button in attachments
    $('#forid').empty();
    var id = `
    <button class="btn btn-success" type="submit" id="attachFile" onclick="attachUpload('${employee.id}')" >Upload File</button>
    `;
    $('#forid').append(id);

    // This will load attachment datatable
    var emp_attachment_url = "/api/hrm/attachments/" + employee.id + "/?format=json";
    attachment_table.api().ajax.url(emp_attachment_url).load();

// Update profile picture
    var ProfilePictureUpload = document.getElementById("ProfilePictureUpload");
    var profile_pic = document.getElementById("profile_pic");
    profile_pic.onclick = function () {
        ProfilePictureUpload.click();
    };
    ProfilePictureUpload.onchange = function () {
        var file = ProfilePictureUpload.files[0];
        var formData = new FormData();
        formData.append('photo', file);
        $.ajax({
            type: "PUT",
            url: "/api/hrm/employee/" + employee.id + "/",
            data: formData, // serializes the form's elements.
            contentType: false, // NEEDED, DON'T OMIT THIS (requires jQuery 1.6+)
            processData: false, // NEEDED, DON'T OMIT THIS
            success: function (data, xhr, formData) {
                $("#profile_pic").attr('src', data['photo'])
                swal({
                       title: "Good job!",
                       text: "Picture Uploaded Successfully!",
                       icon: "success",
                     });
            }
        });
    };
}


function updateGeneral(employee_id) {
    $("#update_general").on("submit", function (event) {
        event.preventDefault();
        $.ajax({
            url: '/api/hrm/employee/' + employee_id + '/',
            type: "PUT",
            data: {
                "work_email": $("#general_email").val(),
                "full_name": $("#general_full_name").val(),
                "first_name": $("#general_first_name").val(),
                "last_name": $("#general_last_name").val(),
                "date_of_birth": $("#general_dob").val(),
                "phone": $("#general_phone").val(),
                "alt_phone": $("#general_alt_phone").val(),
                "gender": $("#general_gender option:selected").val(),
                "marital_status": $("#general_marital option:selected").val(),
                "blood_group": $("#general_blood").val(),
                "pan_number": $("#general_pan").val(),
            },
            success: function (data) {
             success();
            },
            error: function (data) {
              error();
            },
        });
    })
}

// Update Employee Contact Data
function updateContact(employee_id) {
    $("#update_contact").on("submit", function (event) {
        event.preventDefault();
        $.ajax({
            url: '/api/hrm/employee/' + employee_id + '/',
            type: "PUT",
            data: {
                "address_1": $("#contact_address_1").val(),
                "address_2": $("#contact_address_2").val(),
                "address_city": $("#contact_address_city").val(),
                "address_state": $("#contact_address_state").val(),
                "address_zip": $("#contact_address_zip").val(),
                "address_country": $("#contact_address_country").val(),
                "c_address_1": $("#contact_c_address_1").val(),
                "c_address_2": $("#contact_c_address_2").val(),
                "c_address_city": $("#contact_c_address_city").val(),
                "c_address_state": $("#contact_c_address_state").val(),
                "c_address_zip": $("#contact_c_address_zip").val(),
                "c_address_country": $("#contact_c_address_country").val()
            },
            success: function (data) {
              success();
            },
            error: function (data,xhr) {
              error();
            },
        });
    })
}

// Update Employee Job Data
function updateJob(employee_id) {
    $("#update_job").on("submit", function (event) {
        event.preventDefault();
        $.ajax({
            url: '/api/hrm/employee/' + employee_id + '/',
            type: "PUT",
            data: {
                "employee_id": $("#job_employee_id").val(),
                "joining_date": $("#job_doj").val(),
                "designation": $("#job_designation option:selected").val(),
                "department": $("#job_department option:selected").val(),
                "location": $("#job_location option:selected").val(),
                "role": $("#job_role option:selected").val(),
                "employment_status": $("#job_employement_status option:selected").val(),
                "resign_date": $("#job_dor").val(),
            },
            success: function (data) {
                success();
            },
            error: function (data) {
                error();
            },
        });
    })
}

//update bank details
function updateBank(employee_id) {
    $("#update_bank").on("submit", function (event) {
        event.preventDefault();
        $.ajax({
            url: '/api/hrm/employee/' + employee_id + '/',
            type: "PUT",
            data: {
                "bank_name": $("#bank_name").val(),
                "account_number": $("#bank_account").val(),
                "ifsc_code": $("#bank_ifsc").val(),
                "bank_branch": $("#bank_branch").val()
            },
            success: function (data) {
             success();
            },
            error: function (data) {
              error();
            },
        });
    })
}
// Attachment upload function
function attachUpload(employee_id) {
    var table = $('#attachments_datatable').DataTable();
    var fileupload = document.getElementById("file");
    var file = fileupload.files[0];
    var formData = new FormData();
    formData.append('file', file);
    formData.append('attachment_type', $("#fileType option:selected").val());
    formData.append('employee', employee_id);
    $("#update_attach").on("submit", function (event) {
        event.preventDefault();
        $('#update_attach')[0].reset();
        $.ajax({
            type: "POST",
            url: "/api/hrm/attachments/",
            data: formData, // serializes the form's elements.
            contentType: false, // NEEDED, DON'T OMIT THIS (requires jQuery 1.6+)
            processData: false, // NEEDED, DON'T OMIT THIS
            success: function (data) {
                table.ajax.reload();
                formData.forEach(function (val, key, fD) {
                    formData.delete(key)
                });
            },
        });
    })

}

// Deleting Selected Attachments
function delAttach(id) {
    var table = $('#attachments_datatable').DataTable();
    $.ajax({
        type: "DELETE",
        url: "/api/hrm/attachments/delete/" + id + "/",
        contentType: false, // NEEDED, DON'T OMIT THIS (requires jQuery 1.6+)
        processData: false, // NEEDED, DON'T OMIT THIS
        success: function (data) {
            table.ajax.reload();
        },
        error: function (data) {
            error();
        },
    });
}

// Attendance Tab Function
function PopulateAttendanceTab(employee) {
    var today = new Date();
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = today.getFullYear();

    today = yyyy + '-' + mm + '-' + dd;
    document.getElementById("selected_date_display").innerHTML = today;
    LoadDailyWorkhours(employee.employee_id, today);
}

function LoadDailyWorkhours(employee_username, date) {
    // This function will load daily work hours in attendance tab
    var attendance_data = $.ajax({
        url: '/api/work-hours/' + employee_username + '/?date=' + date,
        type: "GET",
        dataType: "application/json",
        async: false,
        error: function (xhr, status, errorThrown) {
            document.getElementById("total").innerHTML = "No Data Found";
            document.getElementById("actual").innerHTML = "No Data Found";
            document.getElementById("non_completed").innerHTML = "No Data Found";
            document.getElementById("additional").innerHTML = "No Data Found"
        }
    }).responseText;
    try{
        var attendance = JSON.parse(attendance_data);
    }
    catch(err){
        alert("No Data Found. \n Please click on dates before today except leaves.")
    }
    document.getElementById("total").innerHTML = attendance['total'];
    document.getElementById("actual").innerHTML = attendance['actual'];
    document.getElementById("non_completed").innerHTML = attendance['non_completed'];
    document.getElementById("additional").innerHTML = attendance['additional']
}

function DailyInOutInformation(employee_username, date) {
    var day_data_json = $.ajax({
        url: '/api/attendance/' + employee_username + '/',
        data: {"date": date},
        type: "POST",
        async: false,
        success: function (msg) {
        },
        error: function (xhr, status, errorThrown) {
        }
    }).responseText;
    var day_data = JSON.parse(day_data_json);
    var excepted_doors = ["MAINDOOR", "PANTRY", "MAINDOOR +"];
    day_data.forEach(function(data, i){
        if (day_data[i]['event_description'] == "Regularisation"){
            var row = `
                <tr bgcolor="#1E88E5" style="color: #fff; font-weight:bold;">
                    <td>${data['event_type']}</td>
                    <td>${data['event_point']}</td>
                    <td>${data['event_time']}</td>
                    <td>${data['event_description']}</td>
                </tr>
                `;
            $("#day_info_tbody").append(row);
            return;
        }
        if ((day_data[i]['event_type'] == 'in' && day_data[i + 1]['event_point'] == day_data[i]['event_point'] && day_data[i + 1]['event_type'] == 'out') || (day_data[i]['event_type'] == 'out' && day_data[i - 1]['event_point'] == day_data[i]['event_point'] && day_data[i - 1]['event_type'] == 'in')){
            var row = `
                <tr bgcolor="#8BC34A" style="color: #fff; font-weight:bold;">
                    <td>${data['event_type']}</td>
                    <td>${data['event_point']}</td>
                    <td>${data['event_time']}</td>
                    <td>${data['event_description']}</td>
                </tr>
                `;
            $("#day_info_tbody").append(row);
        }
        else if (excepted_doors.includes(day_data[i]['event_point']) == true){
            var row = `
                <tr>
                    <td>${data['event_type']}</td>
                    <td>${data['event_point']}</td>
                    <td>${data['event_time']}</td>
                    <td>${data['event_description']}</td>
                </tr>
                `;
            $("#day_info_tbody").append(row);
        }
        else{
            var row = `
                <tr bgcolor="#FF5252" style="color: #fff; font-weight:bold;">
                    <td>${data['event_type']}</td>
                    <td>${data['event_point']}</td>
                    <td>${data['event_time']}</td>
                    <td>${data['event_description']}</td>
                    <td><button class="btn btn-danger">!</button></td>
                </tr>
                `;
               $("#day_info_tbody").append(row);
        }
    });
    $("#day_info_modal").modal('show');
}

function LoadMonthlyWorkhours(employee_username, month) {

    var attendance_data = $.ajax({
        url: '/api/work-hours/' + employee_username + '/?month=' + month,
        type: "GET",
        dataType: "application/json",
        async: false
    }).responseText;
    var attendance = JSON.parse(attendance_data);
    //total
    document.getElementById("total_default").innerHTML = attendance['total']['default']
    document.getElementById("total_target").innerHTML = attendance['total']['target']
    document.getElementById("total_current").innerHTML = attendance['total']['current']
    document.getElementById("total_incomplete").innerHTML = attendance['total']['incomplete']
    //actual
    document.getElementById("actual_default").innerHTML = attendance['actual']['default']
    document.getElementById("actual_target").innerHTML = attendance['actual']['target']
    document.getElementById("actual_current").innerHTML = attendance['actual']['current']
    document.getElementById("actual_incomplete").innerHTML = attendance['actual']['incomplete']
    //days
    document.getElementById("days_default").innerHTML = attendance['days']['default']
    document.getElementById("days_target").innerHTML = attendance['days']['target']
    document.getElementById("days_incomplete").innerHTML = attendance['days']['incomplete']
}

<!-- Initialize Attendance CALENDAR -->
document.addEventListener('DOMContentLoaded', function () {
    var calendarEl = document.getElementById('attendance-calendar');
    var selected_month = 0;
    var calendar = new FullCalendar.Calendar(calendarEl, {
        plugins: ['interaction', 'dayGrid'],
        editable: true,
        selectable: true,
        dateClick: function (info) {
            var selected_date = moment(info.date).format('YYYY-MM-DD');
            var cur_month = selected_date.split("-")[1];
            document.getElementById("selected_date_display").innerHTML = selected_date;
            if (selected_month !== cur_month) {
                selected_month = cur_month;
                LoadMonthlyWorkhours(employee.employee_id, selected_date);
            }
            LoadDailyWorkhours(employee.employee_id, selected_date);
        }
    });
    calendar.render();

    // This will render calender on tab click
    $('.profile-tab, .nav-tabs li').click(function () {
        calendar.render();
    });

});

// Shows Success Message
function success(){
  swal({
         title: "Good job!",
         text: "Updated Successfully!",
         icon: "success",
       });
}
// Shows Error Message
function error(){
  swal({
    title: "Oh!!",
    text: "Something Went Wrong",
    icon: "error",
       });
}
