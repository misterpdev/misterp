#!/usr/bin/env bash
# "Deleting migration files"
python3 scripts/clear_migrations.py

# "BUILDING TABLES"
python3 manage.py makemigrations

# "MIGRATION DATABASE"
python3 manage.py migrate

# "DATABASE BUILD SUCESSFULLY"

# "LOADING DATA FROM FIXTURES"
python3 manage.py loaddata --app auth auth.json
python3 manage.py loaddata --app authtoken authtoken.json
python3 manage.py loaddata --app hrm hrm.json
python3 manage.py loaddata --app production production.json
# "DATA ADDED SUCESSFULLY"

# "Super user details - Username: admin,  Password: mist1234"