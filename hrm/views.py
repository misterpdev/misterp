from django.shortcuts import render
from django.core.exceptions import ObjectDoesNotExist

from .models import *


def employee_directory(request):
    """
    Employee Directory View
    :param request:
    :return:
    """
    queryset = Employee.objects.all()
    departments = Department.objects.all()
    locations = Location.objects.all()
    designations = Designation.objects.all()
    roles = Role.objects.all()
    attachments = AttachmentType.objects.all()
    attachmentsFile = AttachmentFile.objects.all()
    employment_status = EmploymentStatus.objects.all()
    try:
        entitlements = LeaveEntitlement.objects.get(employee=request.user.id)
    except ObjectDoesNotExist:
        entitlements = None
    context = {
        'employee': queryset,
        'department': departments,
        'location': locations,
        'designation': designations,
        'role': roles,
        'attachments':attachments,
        'attachmentsFile':attachmentsFile,
        'entitlements': entitlements,
        'employment_status': employment_status,
    }

    return render(request, 'hrm/directory.html', context)


def leaves(request):
    leaves = Leave.objects.all()
    employee = Employee.objects.all()
    leave_status = LeaveStatus.objects.all()
    departments = Department.objects.all()
    designations = Designation.objects.all()
    try:
        entitlements = LeaveEntitlement.objects.get(employee=request.user.id)
    except ObjectDoesNotExist:
        entitlements = None
    context = {
        'leaves': leaves,
        'employee': employee,
        'leave_status': leave_status,
        'department': departments,
        'designation': designations,
        'entitlements': entitlements
    }
    return render(request, 'hrm/leaves.html', context)


def my_record(request):
    queryset = Employee.objects.all()
    departments = Department.objects.all()
    locations = Location.objects.all()
    designations = Designation.objects.all()
    roles = Role.objects.all()
    attachments = AttachmentType.objects.all()
    attachmentsFile = AttachmentFile.objects.all()
    employment_status = EmploymentStatus.objects.all()
    context = {
        'employee': queryset,
        'department': departments,
        'location': locations,
        'designation': designations,
        'role': roles,
        'attachments':attachments,
        'attachmentsFile':attachmentsFile,
        'self_profile': True,
        'user_profile': request.user,
        'employment_status': employment_status
    }
    return render(request, 'hrm/my_record.html', context=context)

def attendance(request):
    queryset = Employee.objects.all()
    departments = Department.objects.all()
    locations = Location.objects.all()
    designations = Designation.objects.all()
    context = {
        'employee': queryset,
        'department': departments,
        'location': locations,
        'designation': designations,
    }
    return render(request, 'hrm/attendance.html', context=context)

def holidays(request):
    queryset = Employee.objects.all()
    locations = Location.objects.all()
    context = {
        'employee': queryset,
        'user_profile': request.user,
        'location': locations,
    }
    return render(request, 'hrm/holidays.html', context=context)
