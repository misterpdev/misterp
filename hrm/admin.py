from django.contrib import admin

# Register your models here.
from hrm.models import *


class EmployeeFields(admin.ModelAdmin):
    list_display = [f.name for f in Employee._meta.fields]
    list_display.insert(0, 'profile_photo')
    search_fields = ('employee_id', 'full_name')
    exclude = ("profile",)


admin.site.register(Employee, EmployeeFields)
admin.site.register(Department)
admin.site.register(Designation)
admin.site.register(Location)
admin.site.register(EmploymentStatus)
admin.site.register(Skill)
admin.site.register(Level)
admin.site.register(Role)
admin.site.register(LeaveStatus)
admin.site.register(LeaveEntitlement)
admin.site.register(AttachmentType)
admin.site.register(AttachmentFile)
admin.site.register(Holidays)


class LeaveFields(admin.ModelAdmin):
    list_display = [f.name for f in Leave._meta.fields]


admin.site.register(Leave, LeaveFields)
