from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from .serializers import *
from datetime import datetime
from common.notifications import send_email


class EmployeeList(APIView):
    """
    This will return full employee list
    """
    def get(self, request, format=None):
        serializer = EmployeeDetailSerializer(Employee.objects.all(), many=True)
        return Response(serializer.data)


class EmployeeDetailEdit(APIView):
    """
    This is for edit employee detail
    """
    def get(self, request, employee_id, format=None):
        employee = Employee.objects.get(id=employee_id)
        serializer = EmployeeEditSerializer(employee)
        return Response(serializer.data)

    def put(self, request, employee_id):
        employee = Employee.objects.get(id=employee_id)

        serializer = EmployeeEditSerializer(employee, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class AttachmentsAPI(APIView):
    """
    This will return all Attachments Data
    """

    def get(self, request, attachment_id=None, format=None):
        serializer = AttachmentsSerializer(AttachmentFile.objects.all(), many=True)
        return Response(serializer.data)

    def post(self, request, attachment_id=None, format=None):
        serializer = CreateAttachmentsSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, employee, attachment_id, format=None):
        model_object = AttachmentFile.objects.filter(employee=employee, id=attachment_id)
        model_object.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class AttachmentsEdit(APIView):
    """
    This will edit selected Attachment
    """

    def get(self, request, employee_id, format=None):
        serializer = AttachmentsSerializer(AttachmentFile.objects.filter(employee=employee_id), many=True)
        for dt in serializer.data:
            try:
                dt['creation_date'] = datetime.strptime(dt['creation_date'].replace('T', ' ').replace('Z', ''),
                                                        "%Y-%m-%d %H:%M:%S.%f").strftime("%d-%m-%Y")
            except Exception as e:
                print(e)
        return Response(serializer.data)


class AttachmentsDelete(APIView):
    """
    This will edit selected Attachment
    """

    def get(self, request, attachment_id, format=None):
        serializer = AttachmentsSerializer(AttachmentFile.objects.filter(id=attachment_id), many=True)
        for dt in serializer.data:
            try:
                dt['creation_date'] = datetime.strptime(dt['creation_date'].replace('T', ' ').replace('Z', ''),
                                                        "%Y-%m-%d %H:%M:%S.%f").strftime("%d-%m-%Y")
            except Exception as e:
                print(e)
        return Response(serializer.data)

    def delete(self, request, attachment_id, format=None):
        model_object = AttachmentFile.objects.get(id=attachment_id)
        model_object.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class LeavesAPI(APIView):
    """
    This will return all Leaves Data
    """

    def get(self, request, format=None):
        try:
            # checks if user in group
            user = User.objects.get(id=request.user.id)
            groups = [group[0] for group in user.groups.values_list('name')]
            if 'HR Assistant' in groups:
                serializer = LeavesSerializer(Leave.objects.all(), many=True)
            elif 'Leads' in groups:
                serializer = LeavesSerializer(Leave.objects.all(), many=True)
            elif 'Production' in groups:
                serializer = LeavesSerializer(Leave.objects.all(), many=True)
            else:
                serializer = LeavesSerializer(Leave.objects.filter(employee_id=request.user.id), many=True)
        except Exception as e:
            pass
        for dt in serializer.data:
            dt['start_date'] = datetime.strptime(dt['start_date'].replace('T', ' ').replace('Z', ''),
                                                 "%Y-%m-%d %H:%M:%S").strftime("%Y-%m-%d %H:%M:%S")
            dt['end_date'] = datetime.strptime(dt['end_date'].replace('T', ' ').replace('Z', ''),
                                               "%Y-%m-%d %H:%M:%S").strftime("%Y-%m-%d %H:%M:%S")
        return Response(serializer.data)

    def post(self, request, format=None):
        data = request.data
        serializer = CreateLeavesSerializer(data=data)
        if serializer.is_valid():
            instance = serializer.save()
            ##########################################################
            # EMAIL NOTIFICATIONS START
            subject = f"New leave request from {instance.employee.full_name}"
            email_text = f"{instance.employee.full_name} is applied for a new leave"
            html_content = f"""
                        <h3 style="color: #0b9ac4">New leave applied by {instance.employee.full_name}</h3>
                        <p> Reason :  {instance.reason} </p>
                        <p> Duration : {instance.duration} </p>
                        <p> EL Days :{instance.el_days}</p>
                        <p> CL Days :{instance.cl_days}</p>
                        <p> LOP Days :{instance.lop_days}</p>
                        <p> Start Date : {instance.start_date} </p>
                        <p> End Date :{instance.end_date}</p>
                        <p> Applied on  :{instance.creation_date}</p>
                        """
            to = [instance.employee.work_email,
                  'wasim.afser@gmail.com',
                  'narendhar.c.c@gmail.com'
                  ]
            send_email(subject, email_text, html_content, to)
            # EMAIL NOTIFICATIONS END
            ##########################################################
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class LeaveEmployeeEdit(APIView):
    """
    This will edit selected leave
    """

    def get(self, request, employee_id, format=None):
        month = datetime.now().year
        employee = Leave.objects.filter(employee_id=employee_id, start_date__year=month)
        serializer = LeavesEditSerializer(employee, many=True)
        return Response(serializer.data)

    def put(self, request, leave_id):
        employee = Leave.objects.get(id=leave_id)
        serializer = LeavesEditSerializer(employee, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class LeaveDetailEdit(APIView):
    """
    This will edit selected leave
    """

    def get(self, request, leave_id, format=None):
        employee = Leave.objects.get(id=leave_id)
        serializer = LeavesEditSerializer(employee)
        return Response(serializer.data)

    def put(self, request, leave_id):
        employee = Leave.objects.get(id=leave_id)
        serializer = LeavesEditSerializer(employee, data=request.data)
        if serializer.is_valid():
            serializer.save()
            try:
                return Response(serializer.data, status=status.HTTP_201_CREATED)
            except Exception as e:
                print(e)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class LeaveEntitlementAPI(APIView):
    """
    Add employee leaves entitlements
    """

    def get(self, request, format=None):
        serializer = LeavesEntitlementSerializer(LeaveEntitlement.objects.all(), many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        serializer = LeavesEntitlementSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class LeaveEntitlementEdit(APIView):
    """
    This will edit leave entitlement
    """

    def get(self, request, employee_id, format=None):
        employee = LeaveEntitlement.objects.get(employee=employee_id)
        serializer = LeavesEntitlementSerializer(employee)
        return Response(serializer.data)

    def put(self, request, employee_id):
        employee = LeaveEntitlement.objects.get(employee=employee_id)
        serializer = LeavesEntitlementSerializer(employee, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class HolidaysAPI(APIView):
    def get(self, request, location, format=None):
        month = request.GET.get('month', False)
        serializer = HolidaysSerializer(Holidays.objects.filter(location=location, start_date__month=month), many=True)
        return Response(serializer.data)

    def post(self, request, location, format=None):
        serializer = HolidaysSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
