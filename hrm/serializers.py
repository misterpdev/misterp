from rest_framework import serializers
from .models import *


class EmployeeDetailSerializer(serializers.ModelSerializer):
    entitlements = serializers.PrimaryKeyRelatedField(queryset=LeaveEntitlement.objects.all(), required=False)

    class Meta:
        model = Employee
        fields = '__all__'
        depth = 1


class EmployeeEditSerializer(serializers.ModelSerializer):
    department = serializers.SlugRelatedField(queryset=Department.objects.all(), slug_field='name', required=False)
    designation = serializers.SlugRelatedField(queryset=Designation.objects.all(), slug_field='name', required=False)
    location = serializers.SlugRelatedField(queryset=Location.objects.all(), slug_field='name', required=False)
    role = serializers.SlugRelatedField(queryset=Role.objects.all(), slug_field='name', required=False)
    level = serializers.SlugRelatedField(queryset=Level.objects.all(), slug_field='name', required=False)
    employment_status = serializers.SlugRelatedField(queryset=EmploymentStatus.objects.all(), slug_field='name',
                                                     required=False)

    class Meta:
        model = Employee
        fields = '__all__'


class EmployeeCompactSerializer(serializers.ModelSerializer):
    department = serializers.SlugRelatedField(queryset=Department.objects.all(), slug_field='name', required=False)
    designation = serializers.SlugRelatedField(queryset=Designation.objects.all(), slug_field='name', required=False)
    location = serializers.SlugRelatedField(queryset=Location.objects.all(), slug_field='name', required=False)

    class Meta:
        model = Employee
        fields = ('id',
                  'employee_id',
                  'photo',
                  'full_name',
                  'first_name',
                  'last_name',
                  'department',
                  'designation',
                  'location',
                  )
        depth = 1

class AttachmentsSerializer(serializers.ModelSerializer):
    employee = serializers.SlugRelatedField(queryset=Employee.objects.all(), slug_field='id', required=False)
    attachment_type = serializers.SlugRelatedField(queryset=AttachmentType.objects.all(), slug_field='name', required=False)

    class Meta:
        model = AttachmentFile
        fields = '__all__'
        depth = 1


class CreateAttachmentsSerializer(serializers.ModelSerializer):
    employee = serializers.PrimaryKeyRelatedField(queryset=Employee.objects.all(), required=False)
    attachment_type = serializers.SlugRelatedField(queryset=AttachmentType.objects.all(), slug_field='name', required=False)

    class Meta:
        model = AttachmentFile
        fields = '__all__'

class LeavesSerializer(serializers.ModelSerializer):
    employee = EmployeeCompactSerializer(read_only=True)

    class Meta:
        model = Leave
        fields = '__all__'
        depth = 1


class CreateLeavesSerializer(serializers.ModelSerializer):
    employee = serializers.PrimaryKeyRelatedField(queryset=Employee.objects.all(), required=False)

    class Meta:
        model = Leave
        fields = '__all__'


class LeavesEditSerializer(serializers.ModelSerializer):
    employee = EmployeeCompactSerializer(read_only=True, required=False)
    reason = serializers.StringRelatedField(required=False)
    leave_status = serializers.PrimaryKeyRelatedField(queryset=LeaveStatus.objects.all(), required=False)
    start_date = serializers.DateTimeField(required=False)
    end_date = serializers.DateTimeField(required=False)
    el_days = serializers.FloatField(required=False)
    cl_days = serializers.FloatField(required=False)
    lop_days = serializers.FloatField(required=False)
    duration = serializers.FloatField(required=False)

    class Meta:
        model = Leave
        fields = '__all__'


class LeavesEntitlementSerializer(serializers.ModelSerializer):
    start_date = serializers.DateField(required=False)
    end_date = serializers.DateField(required=False)
    el_leaves = serializers.FloatField(required=False)
    cl_leaves = serializers.FloatField(required=False)

    class Meta:
        model = LeaveEntitlement
        fields = '__all__'

class HolidaysSerializer(serializers.ModelSerializer):
    class Meta:
        model = Holidays
        fields = '__all__'
