from django.conf.urls import url
from . import api, views

urlpatterns = [
    url(r'^hrm/employee/$', views.employee_directory, name='employee_list'),
    url(r'^hrm/leaves/$', views.leaves, name='leaves'),
    url(r'^hrm/my_record/$', views.my_record, name='my_record'),
    url(r'^hrm/attendance/$', views.attendance, name='attendance'),
    url(r'^hrm/holidays/$', views.holidays, name='holidays'),
    # API URL
    url(r'^api/hrm/employee/$', api.EmployeeList.as_view(), name='employee_list'),
    url(r'^api/hrm/employee/(?P<employee_id>\d+)/$', api.EmployeeDetailEdit.as_view(), name='employee_edit'),
    # Attachments
    url(r'^api/hrm/attachments/$', api.AttachmentsAPI.as_view(), name='attachments_api'),
    url(r'^api/hrm/attachments/(?P<employee_id>\d+)/$', api.AttachmentsEdit.as_view(), name='attachments_edit'),
    url(r'^api/hrm/attachments/delete/(?P<attachment_id>\d+)/$', api.AttachmentsDelete.as_view(), name='attachments_delete'),
    # Leaves URL
    url(r'^api/hrm/leaves/employee/$', api.LeavesAPI.as_view(), name='leave_api'),
    url(r'^api/hrm/leaves/employee/(?P<employee_id>\d+)/$', api.LeaveEmployeeEdit.as_view(), name='leave_api'),
    url(r'^api/hrm/leaves/(?P<leave_id>\d+)/$', api.LeaveDetailEdit.as_view(), name='leave_edit'),
    url(r'^api/hrm/leaves/entitlement/$', api.LeaveEntitlementAPI.as_view(),name='leave_entitlement'),
    url(r'^api/hrm/leaves/entitlement/(?P<employee_id>\d+)/$', api.LeaveEntitlementEdit.as_view(), name='leave_entitlement_edit'),
    #Holidays URL
    url(r'^api/hrm/holidays/(?P<location>\S+)/$', api.HolidaysAPI.as_view(), name='holidays_api'),

]
