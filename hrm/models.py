from django.db import models
from imagekit.models import ProcessedImageField
from imagekit.processors import ResizeToFill
from django.db.models.signals import post_save
from django.contrib.auth.models import User
from django.dispatch import receiver
from django.utils.safestring import mark_safe
from profiles.models import Profile


# Create your models here.
class EmploymentStatus(models.Model):
    name = models.CharField(max_length=100, unique=True)

    def __str__(self):
        return self.name


class Designation(models.Model):
    name = models.CharField(max_length=100, unique=True)

    def __str__(self):
        return self.name


class Department(models.Model):
    name = models.CharField(max_length=100, unique=True)

    def __str__(self):
        return self.name


class Skill(models.Model):
    name = models.CharField(max_length=100, unique=True)

    def __str__(self):
        return self.name


class Location(models.Model):
    name = models.CharField(max_length=100, unique=True)

    def __str__(self):
        return self.name


class Role(models.Model):
    name = models.CharField(max_length=100, unique=True)

    def __str__(self):
        return self.name


class Level(models.Model):
    name = models.CharField(max_length=100, unique=True)

    def __str__(self):
        return self.name


class Employee(models.Model):
    """
    User Profile
    """
    profile = models.OneToOneField(Profile, on_delete=models.CASCADE, null=True)
    employee_id = models.CharField(max_length=100, null=True, unique=True, blank=True)
    full_name = models.CharField(max_length=200, null=True, blank=True)
    first_name = models.CharField(max_length=100, null=True, blank=True)
    last_name = models.CharField(max_length=100, null=True, blank=True)

    def upload_photo_dir(self, filename):
        ext = filename.split('.')[-1]
        path = 'profiles/photo/{}.{}'.format(self.profile.user.username, ext)
        return path

    photo = ProcessedImageField(upload_to=upload_photo_dir,
                                processors=[ResizeToFill(400, 400)],
                                format='JPEG',
                                options={'quality': 80},
                                null=True
                                )

    date_of_birth = models.DateField(null=True, blank=True)
    gender_choice = (
        ('male', 'Male'),
        ('female', 'Female')
    )
    gender = models.CharField(max_length=10, choices=gender_choice, null=True, blank=True)
    marital_choice = (
        ('single', 'Single'),
        ('married', 'Married')
    )
    pan_number = models.CharField(max_length=200, null=True, blank=True)
    blood_group = models.CharField(max_length=200, null=True, blank=True)
    marital_status = models.CharField(max_length=10, choices=marital_choice, null=True, blank=True)
    # contact details
    address_1 = models.CharField(max_length=200, null=True, blank=True)
    address_2 = models.CharField(max_length=200, null=True, blank=True)
    address_city = models.CharField(max_length=100, null=True, blank=True)
    address_state = models.CharField(max_length=100, null=True, blank=True)
    address_zip = models.CharField(max_length=100, null=True, blank=True)
    address_country = models.CharField(max_length=100, null=True, blank=True)
    # correspondence details
    c_address_1 = models.CharField(max_length=200, null=True, blank=True)
    c_address_2 = models.CharField(max_length=200, null=True, blank=True)
    c_address_city = models.CharField(max_length=100, null=True, blank=True)
    c_address_state = models.CharField(max_length=100, null=True, blank=True)
    c_address_zip = models.CharField(max_length=100, null=True, blank=True)
    c_address_country = models.CharField(max_length=100, null=True, blank=True)

    work_email = models.CharField(max_length=100, null=True, blank=True)
    other_email = models.CharField(max_length=100, null=True, blank=True)
    phone = models.CharField(max_length=100, null=True, blank=True)
    alt_phone = models.CharField(max_length=100, null=True, blank=True)
    # Jobs Details
    joining_date = models.DateField(null=True, blank=True)
    resign_date = models.DateField(null=True, blank=True)
    employment_status = models.ForeignKey(EmploymentStatus, on_delete=models.CASCADE, related_name='+', null=True,
                                          blank=True)
    designation = models.ForeignKey(Designation, on_delete=models.CASCADE, related_name='+', null=True, blank=True)
    department = models.ForeignKey(Department, on_delete=models.CASCADE, related_name='+', null=True, blank=True)
    location = models.ForeignKey(Location, on_delete=models.CASCADE, related_name='+', null=True, blank=True)
    role = models.ForeignKey(Role, on_delete=models.CASCADE, related_name='+', null=True, blank=True)
    level = models.ForeignKey(Level, on_delete=models.CASCADE, related_name='+', null=True, blank=True)
    skill = models.ManyToManyField(Skill, blank=True)

    # Bank_Details
    account_number = models.CharField(max_length=200, null=True, blank=True)
    ifsc_code = models.CharField(max_length=200, null=True, blank=True)
    bank_name = models.CharField(max_length=200, null=True, blank=True)
    bank_branch = models.CharField(max_length=200, null=True, blank=True)
    creation_date = models.DateTimeField(auto_now_add=True)
    modified_date = models.DateTimeField(auto_now=True)

    def profile_photo(self):
        if self.photo:
            return mark_safe('<img src="%s" style="width: 45px; height:45px;" />' % self.photo.url)
        else:
            return 'No Image Found'

    profile_photo.short_description = 'Image'

    def __str__(self):
        if self.full_name:
            return self.full_name
        else:
            return self.profile.user.username


class AttachmentType(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name


class AttachmentFile(models.Model):
    """
    Documents attached with employee profile
    """
    employee = models.ForeignKey(Employee, on_delete=models.CASCADE, related_name='+')
    attachment_type = models.ForeignKey(AttachmentType, on_delete=models.CASCADE, related_name='+')

    def upload_dir(self, filename):
        path = 'hrm/docs/{}'.format(filename)
        return path

    file = models.FileField(upload_to=upload_dir)
    creation_date = models.DateTimeField(auto_now_add=True)
    modified_date = models.DateTimeField(auto_now=True)


class LeaveStatus(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name


class Leave(models.Model):
    reason = models.CharField(max_length=200)
    employee = models.ForeignKey(Employee, on_delete=models.CASCADE, related_name='+')
    leave_status = models.ForeignKey(LeaveStatus, on_delete=models.CASCADE, related_name='+')
    el_days = models.FloatField()
    cl_days = models.FloatField()
    lop_days = models.FloatField()
    duration = models.FloatField()
    start_date = models.DateTimeField()
    end_date = models.DateTimeField()
    notes = models.CharField(max_length=200, null=True, blank=True)
    creation_date = models.DateTimeField(auto_now_add=True)
    modified_date = models.DateTimeField(auto_now=True)

    def __str__(self):
        return "{}_{}".format(self.employee.full_name, self.reason)


class LeaveEntitlement(models.Model):
    employee = models.ForeignKey(Employee, on_delete=models.CASCADE, related_name='+')
    el_leaves = models.FloatField(default=0)
    cl_leaves = models.FloatField(default=0)
    start_date = models.DateField()
    end_date = models.DateField()
    creation_date = models.DateTimeField(auto_now_add=True)
    modified_date = models.DateTimeField(auto_now=True)

    def __str__(self):
        return "{}".format(self.employee.full_name)

class Holidays(models.Model):
    location = models.ForeignKey(Location, on_delete=models.CASCADE, related_name='+', to_field='name')
    event_name = models.CharField(max_length=100)
    start_date = models.DateTimeField()
    end_date = models.DateTimeField()
    event_color = models.CharField(max_length=10, null=True, blank=True)


@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        profile = Profile.objects.create(user=instance)
        full_name = "{} {}".format(profile.user.first_name, profile.user.last_name)
        Employee.objects.create(profile=profile,
                                full_name=full_name,
                                first_name=profile.user.first_name,
                                last_name=profile.user.first_name,
                                work_email=profile.user.email,
                                photo='profiles/photo/{}.jpg'.format(profile.user.username))
